<?php
if (!empty(validation_errors())) {
    echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
    $device->name = set_value('name');
    $device->number_of_screen = set_value('number_of_screen');
    $device->start_date = set_value('start_date');
    $device->start_time = set_value('start_time');
    $device->end_date = set_value('end_date');
    $device->end_time = set_value('end_time');
}
if ($device->start_datetime != '') {
    $expStartDatetime = explode(' ', $device->start_datetime);
    $device->start_date = $expStartDatetime[0];
    $device->start_time = $expStartDatetime[1];
}
if ($device->end_datetime != '') {
    $expEndDatetime = explode(' ', $device->end_datetime);
    $device->end_date = $expEndDatetime[0];
    $device->end_time = $expEndDatetime[1];
}
?>
<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css'); ?>">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?php echo base_url('assets/datepicker/bootstrap-datepicker.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/datepicker/bootstrap-timepicker.min.css'); ?>">
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Device Info</h3>
            </div>
            <div class="box-body">
                <form name="frmDevice" id="frmDevice" method="post" action="<?php echo $actionURL; ?>">
                    <input type="hidden" name="id" value="<?php echo ($device->id) ? $device->id : ''; ?>" id="id">
                    <!-- Select location -->
                    <div>
                        <label for="groups">Select Location: </label>
                        <select class="form-control js-example-basic" id="locationSelect" name="location_id">
                            <?php
                            foreach ($locations as $location) {
                                $varSelect = "";
                                if (!empty(set_value('location_id'))) {
                                    if (in_array($location->id, set_value('location_id')))
                                        $varSelect = "selected";
                                } else {
                                    if ($location->id == $device->location_id)
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $location->id . "'>" . $location->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <!-- Select size -->
                    <div>
                        <label for="groups">Select Size: </label>
                        <select class="form-control js-example-basic" id="sizeSelect" name="size_id">
                            <option value=''>--Select size--</option>
                            <?php
                            foreach ($sizes as $size) {
                                $varSelect = "";
                                if (!empty(set_value('size_id'))) {
                                    if (in_array($size->id, set_value('size_id')))
                                        $varSelect = "selected";
                                } else {
                                    if ($size->id == $device->size_id)
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $size->id . "'>" . $size->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <!-- Select tags -->
                    <div>
                        <label for="groups">Select Tags: </label>
                        <select multiple="multiple" class="form-control js-example-basic-multiple" id="tagSelect" name="tag_id[]">
                            <?php
                            $HiddenTags = explode(',', rtrim($device->tag_ids, ','));
                            foreach ($tags as $tag) {
                                $varSelect = "";
                                if (!empty(set_value('tag_id'))) {
                                    if (in_array($tag->id, set_value('tag_id')))
                                        $varSelect = "selected";
                                } else {
                                    if (in_array($tag->id, $HiddenTags))
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $tag->id . "'>" . $tag->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <!-- Device name -->
                    <div class="form-group">
                        <label>Device ID*</label>
                        <input type="text" class="form-control" name="device_id" value="<?php echo ($device->device_id) ? $device->device_id : ''; ?>" id="value" placeholder="Enter Device ID.">
                    </div>
                    <!-- Start Date -->
                    <div class="form-group datepicker">
                        <label>Start Date:</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right datepicker" name="start_date" value="<?php echo ($device->start_date) ? $device->start_date : ''; ?>" id="start_date" placeholder="Select date">
                        </div>
                        <!-- /.input group -->
                    </div>
                    <!-- time Picker -->
                    <div class="form-group">
                        <label>Start Time:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" class="form-control timepicker" name="start_time" value="<?php echo ($device->start_time) ? $device->start_time : ''; ?>" id="start_time" placeholder="Start time (hh:ii)">
                        </div>
                    </div>
                    <!-- Start Date -->
                    <div class="form-group datepicker">
                        <label>End Date:</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right datepicker" name="end_date" value="<?php echo ($device->end_date) ? $device->end_date : ''; ?>" id="end_date" placeholder="Select end date">
                        </div>
                        <!-- /.input group -->
                    </div>
                    <!-- time Picker -->
                    <div class="form-group">
                        <label>End Time:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" class="form-control timepicker" name="end_time" value="<?php echo ($device->end_time) ? $device->end_time : ''; ?>" id="end_time" placeholder="End time (hh:ii)">
                        </div>
                    </div>
                    <!-- Number of screen -->
                    <div class="form-group">
                        <label>Number of screen</label>
                        <input type="text" class="form-control" name="number_of_screen" value="<?php echo ($device->number_of_screen) ? $device->number_of_screen : ''; ?>" id="number_of_screen" placeholder="Enter number of screen.">
                    </div>
                    <!-- Submit & Cancel button -->
                    <div class="box-footer">
                        <button type="button" name="reset" onclick="javascript:back();" class="btn btn-default">Back</button>
                        <button type="submit" name="submit" onclick="javascript:submitForm();" class="btn btn-info pull-right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/select2/select2.min.js'); ?>"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url('assets/datepicker/bootstrap-datepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/datepicker/bootstrap-timepicker.min.js'); ?>"></script>
<script>
//select2 for user selection
    $(".js-example-basic-multiple").select2();
    $(".js-example-basic").select2();
//submitting form on button click.
    function back() {
        window.location.href = '<?php echo site_url('admin/devices'); ?>';
    }
//submitting form on button click.
    function submitForm() {
        $("#frmDevice").submit();
    }
//Date picker
    $('#start_date,#end_date').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    })
//Timepicker
    $('.timepicker').timepicker({
        showInputs: false,
        showMeridian: false
    })
</script>