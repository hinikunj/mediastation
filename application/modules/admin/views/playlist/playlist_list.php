<?php
//postbag creation / updation messages
if (!empty($this->session->flashdata('success'))) {
    echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
} else if (!empty($this->session->flashdata('error'))) {
    echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
}
?>
<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
        <table id="playlist_table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="nosort">Sr.No</th>
                    <th>Playlist Name</th>
                    <th>Video IDs</th>
                    <th>Location IDs</th>
                    <th>Tag IDs</th>
                    <th>Is Active</th>
                    <th class="nosort">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($playlists as $playlist) {
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $playlist->name; ?></td>
                        <td><?php echo $playlist->video_ids; ?></td>
                        <td><?php echo $playlist->location_ids; ?></td>
                        <td><?php echo $playlist->tag_ids; ?></td>
                        <td><?php echo ($playlist->is_active) ? "Yes" : "No"; ?></td>
                        <td>
                            <a href="<?php echo site_url('admin/playlists/edit') . '/' . $playlist->id; ?>" class="fa fa-edit" title="Edit"></a>
                            <a href="<?php echo site_url('admin/playlists/active') . '/' . $playlist->id; ?>" class="fa fa-check-square-o" title="Active this playlist"></a>
                            <a href="<?php echo site_url('admin/playlists/delete') . '/' . $playlist->id; ?>" onclick="return confirm('Are you sure? It will delete playlist!')" class="fa fa-remove" title="Delete"></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<script>
    $(function () {
        $("#playlist_table").DataTable({
            'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
        });
    });
</script>