<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Logs_model', 'log_m');
    }

    // Logs CRUD
    public function index() {
        $this->mTitle = 'Activity logs';
        $this->render('logs/log_listing');
    }

    public function ajax_logs() {
        $data['draw'] = $this->input->get('draw', true);
        $start = $this->input->get('start', true);
        $limit = $this->input->get('length', true);
        $search_term = $this->input->get('search[value]', true);
        $sort_by = $this->input->get('order[0][column]', true);
        $order_by = $this->input->get('order[0][dir]', true);

        $data['data'] = array();

        $data['recordsTotal'] = $this->log_m->fetch_logs(true, false)->row_array()['count']; // get all tokes count
        $data['recordsFiltered'] = $this->log_m->fetch_logs(true, true, $limit, $start, $search_term, $sort_by, $order_by)->row_array()['count']; // get all filtered log count

        $logs = $this->log_m->fetch_logs(false, true, $limit, $start, $search_term, $sort_by, $order_by)->result_array(); // get all log and form in array

        foreach ($logs as $key) {

            $data['data'][] = array(
                "DT_RowId" => "logs_row_id_" . $key['id'],
                "0" => ($key['id']) ? $key['id'] : '-',
                "1" => ($key['activity']) ? $key['activity'] : '-',
                "2" => ($key['link']) ? '<a href="' . $key['link'] . '" class="fa fa-link" title="View module">&nbsp;</a>' : '-',
                    //"3" => '<a href="' . site_url('admin/logs/view') . '/' . $key['id'] . '" class="fa fa-search crud-action" title="View Detail"></a>'
            );
        }
        echo json_encode($data);
    }

    //update status
    public function mark_read() {
        // only top-level users can reset Admin User passwords
        $this->verify_auth(array('webmaster'));

        $log_id = $this->log_m->markRead();
        if ($log_id > 0) {
            // proceed to change status of logs
            $messages = "Activity log has been marked as read.";
            $this->session->set_flashdata('success', $messages);
        } else {
            $errors = "Something went wrong. Please retry.";
            $this->session->set_flashdata('error', $errors);
        }
        redirect('admin/logs');
    }

}
