<?php
if (!empty($this->session->flashdata('success'))) {
    echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
} else if (!empty($this->session->flashdata('error'))) {
    echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
}
?>
<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
        <table id="video_table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="nosort">Sr.No</th>
                    <th>Video Name</th>
                    <th>Video Size</th>
                    <th>Video Length(Sec)</th>
                    <th class="nosort">Preview</th>
                    <th class="nosort">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($videos as $video) {
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $video->name; ?></td>
                        <td><?php echo $video->size; ?></td>
                        <td><?php echo $video->playtime; ?></td>
                        <td>
                            <video width="200" height="180" controls>
                                <source src="<?php echo VIDEO_URL . $video->size_id . '/' . $video->name; ?>" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                        </td>
                        <td>
                            <a href="<?php echo site_url('admin/videos/edit') . '/' . $video->id; ?>" class="fa fa-edit" title="Edit"></a>
                            <a href="<?php echo site_url('admin/videos/delete') . '/' . $video->id; ?>" onclick="return confirm('Are you sure? It will delete video!')" class="fa fa-remove" title="Delete"></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<script>
    $(function () {
        $("#video_table").DataTable({
            'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
        });
    });
</script>