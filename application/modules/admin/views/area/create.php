<?php
if (!empty(validation_errors())) {
    echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
    $area->name = set_value('name');
}
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Area Info</h3>
            </div>
            <div class="box-body">
                <form name="frmArea" id="frmArea" method="post" action="<?php echo $actionURL; ?>">
                    <input type="hidden" name="id" value="<?php echo ($area->id) ? $area->id : ''; ?>" id="id">
                    <!-- Area name -->
                    <div class="form-group">
                        <label>Area Name*</label>
                        <input type="text" class="form-control" name="name" value="<?php echo ($area->name) ? $area->name : ''; ?>" id="value" placeholder="Enter Area Name.">
                    </div>
                    <!-- Submit & Cancel button -->
                    <div class="box-footer">
                        <button type="button" name="reset" onclick="javascript:back();" class="btn btn-default">Back</button>
                        <button type="submit" name="submit" onclick="javascript:submitForm();" class="btn btn-info pull-right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
//submitting form on button click.
    function back() {
        window.location.href = '<?php echo site_url('admin/areas'); ?>';
    }
//submitting form on button click.
    function submitForm() {
        $("#frmArea").submit();
    }
</script>