<?php
//postbag creation / updation messages
if (!empty($this->session->flashdata('success'))) {
    echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
} else if (!empty($this->session->flashdata('error'))) {
    echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
}
?>
<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
        <table id="device_table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="nosort">Sr.No</th>
                     <th>Device ID</th>
                    <th class="nosort">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($devices as $device) {
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $device->device_id; ?></td>
                        <td>
                            <a href="<?php echo site_url('admin/devices/edit') . '/' . $device->id; ?>" class="fa fa-edit" title="Edit"></a>
                            <a href="<?php echo site_url('admin/devices/delete') . '/' . $device->id; ?>" onclick="return confirm('Are you sure? It will delete device!')" class="fa fa-remove" title="Delete"></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<script>
    $(function () {
        $("#device_table").DataTable({
            'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
        });
    });
</script>