<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class States extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('States_model', 'states');
        $this->load->helper('security');
    }

    public function index() {
        $this->mTitle = 'States';
        // get list of users
        $this->mViewData['states'] = $this->states->get_many_by(array('is_delete' => 0));
        $this->render('state/state_list');
    }

    // Create New State
    public function create() {
        $this->form_validation->set_rules('name', 'State name', 'required|callback_unique_state_check');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $insertState_Array = array(
                'name' => $this->input->post('name', TRUE),
                'created_by' => $this->session->userdata('user_id'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            // proceed to create State
            $state_id = $this->states->insert($insertState_Array);
            if ($state_id) {
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " created state Name:" . $this->input->post('name', TRUE) . " ID:" . $state_id,
                    'link' => site_url('admin/states'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */

                // success
                $messages = "State successfully created.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/states');
        }
        $this->mViewData['actionURL'] = site_url('admin/states/create');
        $this->mTitle = 'Create State';
        $this->render('state/create');
    }

    // Edit State
    public function edit($state_id = 0) {
        $this->form_validation->set_rules('name', 'State name', 'required|callback_unique_state_check');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $updateStateArray = array(
                'name' => $this->input->post('name', TRUE),
                'updated_by' => $this->session->userdata('user_id'),
                'updatedtime' => date('Y-m-d H:i:s')
            );
            // proceed to update state
            $updateFlag = $this->states->update($state_id, $updateStateArray);
            if ($updateFlag) {
                // success
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " edited state Name:" . $this->input->post('name', TRUE) . " ID:" . $state_id,
                    'link' => site_url('admin/states'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                $messages = "State successfully updated.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/states');
        }

        if ($state_id != 0) {
            // get list of state
            $this->mViewData['state'] = $this->states->get($state_id);
            $this->mTitle = 'Edit State';
            $this->mViewData['actionURL'] = site_url('admin/states/edit') . '/' . $state_id;
            $this->render('state/create');
        } else {
            redirect('admin/states');
        }
    }

    // Soft Delete state
    public function delete($state_id = 0) {
        // only top-level users can reset user passwords
        $this->verify_auth(array('webmaster', 'admin'));
        // proceed to change user password
        if ($this->states->update($state_id, array('is_delete' => 1, 'updated_by' => $this->session->userdata('user_id')))) {
            /* logging activity */
            $activity = array(
                'user_id' => $this->session->userdata('user_id'),
                'activity' => $this->session->userdata('username') . " deleted state ID:" . $state_id,
                'link' => site_url('admin/states'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            $this->ion_auth->activity_log($activity);
            /* logging activity */
            // success
            $messages = "State successfully deleted.";
            $this->session->set_flashdata('success', $messages);
        } else {
            // failed
            $errors = "Something went wrong. Please retry.";
            $this->session->set_flashdata('error', $errors);
        }
        redirect('admin/states');
    }

    //callback for unique state name
    public function unique_state_check($str) {
        if ($this->input->post('id', TRUE))
            $this->db->where_not_in('id', $this->input->post('id', TRUE));
        $this->db->where('name', $str);
        if ($this->db->count_all_results('states') > 0) {
            $this->form_validation->set_message('unique_state_check', 'The {field} field must contain a unique value.');
            return FALSE;
        } else {
            return true;
        }
    }

}
