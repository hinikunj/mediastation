<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * auth Controller with Swagger annotations
 * Reference: https://github.com/zircote/swagger-php/
 */
class Auth extends API_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Playlist_model', 'playlist_m');
    }

    /**
     * @SWG\Get(
     * 	path="/auth/playlist_get/",
     * 	tags={"Playlist"},
     * 	summary="Playlist list with Details",
     * 	
     * 	@SWG\Response(
     * 		response="200",
     * 		description="Playlist Array"
     * 	),
     *  @SWG\Response(
     * 		response="400",
     * 		description="Bad Request / Missing parameter"
     * 	),
     * )
     */
    public function playlist_post($device_id = false) {
        $message = get_response_format();
        $device_id = $this->input->post('device_id', TRUE);
        //validate required params
        if (!$device_id) {
            $message['error'] = "Missing parameter or its value.";
            log_message('error', 'playlist_post API Error ' . print_r($message['error'], 1));
            $this->response($message, $message['code']);
        }
        $headers = getallheaders();
        $access_token = $headers['x-access-token'];
        $access_tokenDecode = base64_decode($access_token);
        if ($access_tokenDecode != ACCESSTOKEN) {
            $message['error'] = "Unauthorized access-token.";
            log_message('error', 'playlist_post API Error ' . print_r($message['error'], 1));
            $this->response($message, $message['code']);
        }
        if ($access_tokenDecode == ACCESSTOKEN) {
            $playlist_arr = $this->playlist_m->get_all_videos($device_id);
            if (count($playlist_arr) <= 0) {
                $message['error'] = 'No playlist/video assigned to this device.';
                log_message('error', 'playlist_post API Error ' . print_r($message['error'], 1));
                $this->response($message, $message['code']);
            }
            $message = get_response_format(TRUE, 0, 200, array('data' => $playlist_arr));
            log_message('error', 'playlist_post API Success.');
        }
        $this->response($message, $message['code']);
    }

    public function download_post() {
        $message = get_response_format();
        $device_id = $this->input->post('device_id', TRUE);
        $video_id = $this->input->post('video_id', TRUE);
        //validate required params
        if (!$device_id && !$video_id) {
            $message['error'] = "Missing parameter or its value.";
            log_message('error', 'download API Error ' . print_r($message['error'], 1));
            $this->response($message, $message['code']);
        }
        $headers = getallheaders();
        $access_token = $headers['x-access-token'];
        $access_tokenDecode = base64_decode($access_token);
        if ($access_tokenDecode != ACCESSTOKEN) {
            $message['error'] = "Unauthorized access-token.";
            log_message('error', 'download API Error ' . print_r($message['error'], 1));
            $this->response($message, $message['code']);
        }
        if ($access_tokenDecode == ACCESSTOKEN) {
            $download_arr = $this->playlist_m->download_videos($this->input->post());
            if ($download_arr) {
                $message = get_response_format(TRUE, 0, 200, array('data' => "Download status has been set successfully"));
                log_message('error', 'download API Success.');
            } else {
                $message['error'] = 'Something went wrong while adding status.';
                log_message('error', 'download API Error ' . print_r($message['error'], 1));
            }
            $this->response($message, $message['code']);
        }
        $this->response($message, $message['code']);
    }

    public function logout_post() {
        log_message('error', 'logout API Start.');
        $response_data = array();
        $message['code'] = '200';
        $headers = getallheaders();
        $access_token = $headers['X-ACCESS-TOKEN'];

        //validate required params
        if ($access_token == "") {
            $error_msg = array('message' => 'Access token is missing.');
            $message['code'] = '400';
            $response_data = array("errors" => $error_msg, 'status_code' => $message['code']);
        }

        // proceed to login user
        $logout_res = $this->ion_auth->logout();

        $success_msg = array('message' => 'Logged out successfully.');
        $response_data = array("data" => $success_msg, 'status_code' => $message['code']);
        $this->response($response_data, $message['code']);
        log_message('error', 'Logout API End for user id. ' . $this->post('userid'));
    }

}
