<?php
//postbag creation / updation messages
if (!empty($this->session->flashdata('success'))) {
    echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
} else if (!empty($this->session->flashdata('error'))) {
    echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
}
?>
<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
        <table id="location_table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="nosort">Sr.No</th>
                    <th>Location Name</th>
                    <th>Location Price</th>
                    <th class="nosort">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($locations as $location) {
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $location->name; ?></td>
                        <td><?php echo $location->price; ?></td>
                        <td>
                            <a href="<?php echo site_url('admin/locations/edit') . '/' . $location->id; ?>" class="fa fa-edit" title="Edit"></a>
                            <a href="<?php echo site_url('admin/locations/delete') . '/' . $location->id; ?>" onclick="return confirm('Are you sure? It will delete location!')" class="fa fa-remove" title="Delete"></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<script>
    $(function () {
        $("#location_table").DataTable({
            'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
        });
    });
</script>