<?php
//postbag creation / updation messages
if (!empty($this->session->flashdata('success'))) {
    echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
} else if (!empty($this->session->flashdata('error'))) {
    echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
}
?>
<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
        <table id="channelsvideo_table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="nosort">Sr.No</th>
                    <th>video repeat count</th>
                    <th>Time slot</th>
                    <th class="nosort">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($channelsvideos as $channelsvideo) {
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $channelsvideo->video_repeat_count; ?></td>
                        <td><?php echo $channelsvideo->time; ?></td>
                        <td>
                            <a href="<?php echo site_url('admin/channelsvideo/edit') . '/' . $channelsvideo->id; ?>" class="fa fa-edit" title="Edit"></a>
                            <a href="<?php echo site_url('admin/channelsvideo/delete') . '/' . $channelsvideo->id; ?>" onclick="return confirm('Are you sure? It will delete channelsvideo!')" class="fa fa-remove" title="Delete"></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<script>
    $(function () {
        $("#channelsvideo_table").DataTable({
            'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
        });
    });
</script>