<?php
//postbag creation / updation messages
if (!empty($this->session->flashdata('success'))) {
    echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
} else if (!empty($this->session->flashdata('error'))) {
    echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
}
?>
<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
        <table id="channel_table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="nosort">Sr.No</th>
                    <!-- <th>Channel ID</th> -->
                    <th>Channel Name</th>
                    <th>Channel Time(sec)</th>
                    <th class="nosort">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($channels as $channel) {
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <!-- <td><?php #echo $channel->id;  ?></td> -->
                        <td><?php echo $channel->name; ?></td>
                        <td><?php echo $channel->time; ?></td>
                        <td>
                            <a href="<?php echo site_url('admin/channels/edit') . '/' . $channel->id; ?>" class="fa fa-edit" title="Edit"></a>
                            <a href="<?php echo site_url('admin/channels/delete') . '/' . $channel->id; ?>" onclick="return confirm('Are you sure? It will delete channel!')" class="fa fa-remove" title="Delete"></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<script>
    $(function () {
        $("#channel_table").DataTable({
            'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
        });
    });
</script>