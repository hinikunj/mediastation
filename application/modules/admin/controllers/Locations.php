<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Locations extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Locations_model', 'locations');
    }

    public function index() {
        $this->mTitle = 'Locations';
        // get list of users
        $this->mViewData['locations'] = $this->locations->get_many_by(array('is_delete' => 0));
        $this->render('location/location_list');
    }

    // Create New Location
    public function create() {
        $no_time = ($this->input->post('no_time')) ? $this->input->post('no_time') : 0;
        $this->form_validation->set_rules('name', 'Location name', 'required');
        $this->form_validation->set_rules('price', 'Location Price', 'trim|integer');
        if ($this->form_validation->run() == TRUE) {
            // passed validation
            $area_ids = implode(',', $this->input->post('area_id', TRUE));
            $city_ids = implode(',', $this->input->post('city_id', TRUE));
            $state_ids = implode(',', $this->input->post('state_id', TRUE));
            $insertLocation_Array = array(
                'name' => $this->input->post('name', TRUE),
                'price' => $this->input->post('price', TRUE),
                'no_time' => $this->input->post('no_time', TRUE),
                'start_time' => $this->input->post('start_time', TRUE),
                'end_time' => $this->input->post('end_time', TRUE),
                'area_ids' => $area_ids,
                'city_ids' => $city_ids,
                'state_ids' => $state_ids,
                'created_by' => $this->session->userdata('user_id'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            // proceed to create Location
            $location_id = $this->locations->insert($insertLocation_Array);
            if ($location_id) {
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " created location Name:" . $this->input->post('name', TRUE) . " ID:" . $location_id,
                    'link' => site_url('admin/locations'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                // success
                $messages = "Location successfully created.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/locations');
        }
        /* dynamic area */
        $this->load->model('Areas_model', 'areas');
        $this->mViewData['areas'] = $this->areas->get_many_by(array('is_delete' => 0));
        /* dynamic area ends */
        /* dynamic city */
        $this->load->model('Cities_model', 'cities');
        $this->mViewData['cities'] = $this->cities->get_many_by(array('is_delete' => 0));
        /* dynamic city ends */
        /* dynamic state */
        $this->load->model('States_model', 'states');
        $this->mViewData['states'] = $this->states->get_many_by(array('is_delete' => 0));
        /* dynamic state ends */
        $this->mViewData['actionURL'] = site_url('admin/locations/create');
        $this->mTitle = 'Create Location';
        $this->render('location/create');
    }

    // Edit Location
    public function edit($location_id = 0) {
        $no_time = ($this->input->post('no_time')) ? $this->input->post('no_time') : 0;
        $this->form_validation->set_rules('name', 'Location name', 'required');
        $this->form_validation->set_rules('price', 'Location Price', 'trim|integer');
        if ($this->form_validation->run() == TRUE) {
            // passed validation
            $area_ids = implode(',', $this->input->post('area_id', TRUE));
            $city_ids = implode(',', $this->input->post('city_id', TRUE));
            $state_ids = implode(',', $this->input->post('state_id', TRUE));
            $updateLocationArray = array(
                'name' => $this->input->post('name', TRUE),
                'price' => $this->input->post('price', TRUE),
                'no_time' => $this->input->post('no_time', TRUE),
                'start_time' => $this->input->post('start_time', TRUE),
                'end_time' => $this->input->post('end_time', TRUE),
                'area_ids' => $area_ids,
                'city_ids' => $city_ids,
                'state_ids' => $state_ids,
                'updated_by' => $this->session->userdata('user_id'),
                'updatedtime' => date('Y-m-d H:i:s')
            );
            // proceed to update location
            $updateFlag = $this->locations->update($location_id, $updateLocationArray);
            if ($updateFlag) {
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " edited location ID:" . $location_id,
                    'link' => site_url('admin/locations'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                // success
                $messages = "Location successfully updated.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/locations');
        }

        if ($location_id != 0) {
            // get list of location
            $this->mViewData['location'] = $this->locations->get($location_id);
            $this->mTitle = 'Edit Location';
            /* dynamic area */
            $this->load->model('Areas_model', 'areas');
            $this->mViewData['areas'] = $this->areas->get_many_by(array('is_delete' => 0));
            /* dynamic area ends */
            /* dynamic city */
            $this->load->model('Cities_model', 'cities');
            $this->mViewData['cities'] = $this->cities->get_many_by(array('is_delete' => 0));
            /* dynamic city ends */
            /* dynamic state */
            $this->load->model('States_model', 'states');
            $this->mViewData['states'] = $this->states->get_many_by(array('is_delete' => 0));
            /* dynamic state ends */
            $this->mViewData['actionURL'] = site_url('admin/locations/edit') . '/' . $location_id;
            $this->render('location/create');
        } else {
            redirect('admin/locations');
        }
    }

    // Soft Delete location
    public function delete($location_id = 0) {
        // only top-level users can reset user passwords
        $this->verify_auth(array('webmaster', 'admin'));
        // proceed to change user password
        if ($this->locations->update($location_id, array('is_delete' => 1, 'updated_by' => $this->session->userdata('user_id')))) {
            /* logging activity */
            $activity = array(
                'user_id' => $this->session->userdata('user_id'),
                'activity' => $this->session->userdata('username') . " deleted location ID:" . $location_id,
                'link' => site_url('admin/locations'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            $this->ion_auth->activity_log($activity);
            /* logging activity */
            // success
            $messages = "Location successfully deleted.";
            $this->session->set_flashdata('success', $messages);
        } else {
            // failed
            $errors = "Something went wrong. Please retry.";
            $this->session->set_flashdata('error', $errors);
        }
        redirect('admin/locations');
    }

}
