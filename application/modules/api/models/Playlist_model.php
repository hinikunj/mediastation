<?php

/**
 * Letter CRUD Model
 *
 *
 * @package     letter CRUD
 * @author      Nikunj Dhimar
 * @version     1.0.0
 */
class Playlist_model extends CI_Model {

    public function get_all_videos($device_id = false) {
        if (!$device_id)
            return;
        $this->db->select('am.time, am.channel_repeat_count, cv.video_repeat_count, cv.video_ids, c.`name` AS channel_name, c.time, d.tag_ids'
                . ',l.name AS location_name, l.start_time, l.end_time');
        $this->db->from('devices as d');
        $this->db->join('locations as l', 'd.location_id = l.id');
        $this->db->join('assign_master as am', 'd.id = am.device_id');
        $this->db->join('channel_video as cv', 'am.channel_id = cv.channel_id');
        $this->db->join('channels as c', 'cv.channel_id = c.id');
        $where = array('d.device_id =' => $device_id);

        $this->db->where($where);
        $get_data = $this->db->get();
        $get_array = $get_data->row_array();

        if ($get_array['video_ids'] != "") {
            //fetching videos information
            $videoArr = explode(',', $get_array['video_ids']);
            $whereVideo = array('is_delete' => 0);
            $this->db->select('CONCAT_WS("","' . VIDEO_URL . '",IF(videos.size_id IS NOT NULL, videos.size_id, NULL),IF(videos.size_id IS NOT NULL, "/", NULL),videos.name) AS video_url,videos.id,videos.name,videos.playtime');
            $this->db->where_in('id', $videoArr);
            $returnVideo = $this->db->get_where('videos', $whereVideo)->result_array();
            if (count($returnVideo) > 0) {
                $get_array['videos'] = $returnVideo;
                foreach ($returnVideo as $key => $video) {
                    $this->db->select('is_downloaded');
                    $whereArr = array('device_id' => $device_id, 'video_id' => $video['id']);
                    $returnDevice = $this->db->get_where('downloads', $whereArr)->row();
                    if (count($returnDevice) > 0) {
                        $get_array['videos'][$key]['is_downloaded'] = $returnDevice->is_downloaded;
                    } else {
                        $get_array['videos'][$key]['is_downloaded'] = 0;
                    }
                    $get_array['videos'][$key]['video_repeat_count'] = $get_array['video_repeat_count'];
                }
            }
        }
        if ($get_array['tag_ids'] != "") {
            //fetching tags information
            $tagsArr = explode(',', $get_array['tag_ids']);
            $whereTags = array('is_delete' => 0);
            $this->db->select('tags.name');
            $this->db->where_in('id', $tagsArr);
            $returnTags = $this->db->get_where('tags', $whereTags)->result_array();
            if (count($returnTags) > 0) {
                $get_array['tags'] = $returnTags;
            }
        }
        return $get_array;
    }

    public function download_videos($postArr = array()) {
        if (!empty($postArr)) {
            $insertArray = array(
                'device_id' => ($postArr['device_id']) ? $postArr['device_id'] : '',
                'gps_location' => ($postArr['gps_location']) ? $postArr['gps_location'] : '',
                'device_memory' => ($postArr['device_memory']) ? $postArr['device_memory'] : '',
                'video_id' => ($postArr['video_id']) ? $postArr['video_id'] : '',
                'is_downloaded' => ($postArr['is_downloaded']) ? $postArr['is_downloaded'] : 0,
                'is_deleted' => ($postArr['is_deleted']) ? $postArr['is_deleted'] : 0
            );
            if ($postArr['device_id'] != '' && $postArr['video_id'] != '') {
                $this->db->select('id');
                $whereArr = array('device_id' => $postArr['device_id'], 'video_id' => $postArr['video_id']);
                $returnDevice = $this->db->get_where('downloads', $whereArr)->row();
                if (count($returnDevice) > 0) {
                    $this->db->where($whereArr);
                    $insertArray['updatedtime'] = date('Y-m-d H:i:s');
                    $this->db->update('downloads', $insertArray);
                    return true;
                } else {
                    // proceed to create download row
                    $insertArray['createdtime'] = date('Y-m-d H:i:s');
                    $this->db->insert('downloads', $insertArray);
                    //return $this->db->insert_id();
                    return true;
                }
            }
        }
        return false;
    }

    public function check_access_token($access_token) {
        return true;
        $is_access_token = array();
        $is_access_token = $this->db->select('access_token')->where(array('access_token' => $access_token))->get('users')->result_array();
        if (!empty($is_access_token)) {
            return true;
        }
        return false;
    }

}
