<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Tags_model', 'tags');
        $this->load->helper('security');
    }

    public function index() {
        $this->mTitle = 'Tags';
        // get list of users
        $this->mViewData['tags'] = $this->tags->get_many_by(array('is_delete' => 0));
        $this->render('tag/tag_list');
    }

    // Create New Tag
    public function create() {
        $this->form_validation->set_rules('name', 'Tag name', 'required|callback_unique_tag_check');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $insertTag_Array = array(
                'name' => $this->input->post('name', TRUE),
                'created_by' => $this->session->userdata('user_id'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            // proceed to create Tag
            $tag_id = $this->tags->insert($insertTag_Array);
            if ($tag_id) {
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " created tag Name:" . $this->input->post('name', TRUE) . " ID:" . $tag_id,
                    'link' => site_url('admin/tags'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */

                // success
                $messages = "Tag successfully created.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/tags');
        }
        $this->mViewData['actionURL'] = site_url('admin/tags/create');
        $this->mTitle = 'Create Tag';
        $this->render('tag/create');
    }

    // Edit Tag
    public function edit($tag_id = 0) {
        $this->form_validation->set_rules('name', 'Tag name', 'required|callback_unique_tag_check');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $updateTagArray = array(
                'name' => $this->input->post('name', TRUE),
                'updated_by' => $this->session->userdata('user_id'),
                'updatedtime' => date('Y-m-d H:i:s')
            );
            // proceed to update tag
            $updateFlag = $this->tags->update($tag_id, $updateTagArray);
            if ($updateFlag) {
                // success
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " edited tag Name:" . $this->input->post('name', TRUE) . " ID:" . $tag_id,
                    'link' => site_url('admin/tags'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                $messages = "Tag successfully updated.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/tags');
        }

        if ($tag_id != 0) {
            // get list of tag
            $this->mViewData['tag'] = $this->tags->get($tag_id);
            $this->mTitle = 'Edit Tag';
            $this->mViewData['actionURL'] = site_url('admin/tags/edit') . '/' . $tag_id;
            $this->render('tag/create');
        } else {
            redirect('admin/tags');
        }
    }

    // Soft Delete tag
    public function delete($tag_id = 0) {
        // only top-level users can reset user passwords
        $this->verify_auth(array('webmaster', 'admin'));
        // proceed to change user password
        if ($this->tags->update($tag_id, array('is_delete' => 1, 'updated_by' => $this->session->userdata('user_id')))) {
            /* logging activity */
            $activity = array(
                'user_id' => $this->session->userdata('user_id'),
                'activity' => $this->session->userdata('username') . " deleted tag ID:" . $tag_id,
                'link' => site_url('admin/tags'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            $this->ion_auth->activity_log($activity);
            /* logging activity */
            // success
            $messages = "Tag successfully deleted.";
            $this->session->set_flashdata('success', $messages);
        } else {
            // failed
            $errors = "Something went wrong. Please retry.";
            $this->session->set_flashdata('error', $errors);
        }
        redirect('admin/tags');
    }

    //callback for unique tag name
    public function unique_tag_check($str) {
        if ($this->input->post('id', TRUE))
            $this->db->where_not_in('id', $this->input->post('id', TRUE));
        $this->db->where('name', $str);
        if ($this->db->count_all_results('tags') > 0) {
            $this->form_validation->set_message('unique_tag_check', 'The {field} field must contain a unique value.');
            return FALSE;
        } else {
            return true;
        }
    }

}
