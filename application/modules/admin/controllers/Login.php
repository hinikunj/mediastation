<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// NOTE: this controller inherits from MY_Controller instead of Admin_Controller,
// since no authentication is required
class Login extends MY_Controller {

    /**
     * Login page and submission
     */
    public function index() {
        $this->load->library('form_builder');
        $form = $this->form_builder->create_form();

        if ($form->validate()) {
            // passed validation
            $identity = $this->input->post('username');
            $password = $this->input->post('password');
            $remember = ($this->input->post('remember') == 'on');

            if ($this->ion_auth->login($identity, $password, $remember)) {
                // login succeed
                $messages = $this->ion_auth->messages();
                $this->system_message->set_success($messages);
                redirect($this->mModule);
            }
            else {
                // login failed
                $errors = $this->ion_auth->errors();
                $this->system_message->set_error($errors);
                refresh();
            }
        }

        // display form when no POST data, or validation failed
        $this->mViewData['form'] = $form;
        $this->mBodyClass = 'login-page';
        $this->render('login', 'empty');
    }
    
    #temp function for avoid login screen
    public function expressLogin() {
        $query = $this->db->select('username, email, id, password, active, last_login')
                ->limit(1)
                ->get('admin_users');
        $user = $query->row();
        
        $session_data = array(
            'identity' => $user->username,
            'username' => $user->username,
            'email' => $user->email,
            'user_id' => $user->id, //everyone likes to overwrite id so we'll use user_id
            'old_last_login' => $user->last_login,
            'last_check' => time(),
        );
        $this->session->set_userdata($session_data);
        // login succeed
        redirect('admin');
    }
}
