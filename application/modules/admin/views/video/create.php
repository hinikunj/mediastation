<?php
if (!empty(validation_errors())) {
    echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
    $video->name = set_value('name');
}
?>
<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css'); ?>">
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Video Info</h3>
            </div>
            <div class="box-body">
                <form name="frmVideo" id="frmVideo" enc method="post" action="<?php echo $actionURL; ?>" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo ($video->id) ? $video->id : ''; ?>" id="id">
                    <!-- Select size -->
                    <div>
                        <label for="groups">Select Size: </label>
                        <select class="form-control js-example-basic" id="sizeSelect" name="size_id">
                            <option value=''>--Select size--</option>
                            <?php
                            foreach ($sizes as $size) {
                                $varSelect = "";
                                if (!empty(set_value('size_id'))) {
                                    if (in_array($size->id, set_value('size_id')))
                                        $varSelect = "selected";
                                } else {
                                    if ($size->id == $video->size_id)
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $size->id . "'>" . $size->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <!-- Select Video -->
                    <div class="form-group">
                        <label for="postbage_image">Upload Video</label>
                        <input type="file" name="video_file" id="name">
                        <p class="help-block">Select video.</p>
                        <?php
                        if ($video->name != '') {
                            echo '<p class="help-block">' . $video->name . '</p>';
                        }
                        ?>
                        <input type="hidden" name="video_name" id="video_name" value="<?php echo ($video->name) ? $video->name : ''; ?>" />
                    </div>
                    <!-- Select tags -->
                    <div>
                        <label for="groups">Select Tags: </label>
                        <select multiple="multiple" class="form-control js-example-basic-multiple" id="tagSelect" name="tag_id[]">
                            <?php
                            $HiddenTags = explode(',', rtrim($video->tag_ids, ','));
                            foreach ($tags as $tag) {
                                $varSelect = "";
                                if (!empty(set_value('tag_id'))) {
                                    if (in_array($tag->id, set_value('tag_id')))
                                        $varSelect = "selected";
                                } else {
                                    if (in_array($tag->id, $HiddenTags))
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $tag->id . "'>" . $tag->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <!-- Submit & Cancel button -->
                    <div class="box-footer">
                        <button type="button" name="reset" onclick="javascript:back();" class="btn btn-default">Back</button>
                        <button type="submit" name="submit" onclick="javascript:submitForm();" class="btn btn-info pull-right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/select2/select2.min.js'); ?>"></script>
<script>
//select2 for user selection
                            $(".js-example-basic-multiple").select2();
//submitting form on button click.
                            function back() {
                                window.location.href = '<?php echo site_url('admin/videos'); ?>';
                            }
//submitting form on button click.
                            function submitForm() {
                                $("#frmVideo").submit();
                            }
</script>