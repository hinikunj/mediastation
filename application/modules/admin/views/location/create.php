<?php
if (!empty(validation_errors())) {
    echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
    $location->name = set_value('name');
}
?>
<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css'); ?>">
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Location Info</h3>
            </div>
            <div class="box-body">
                <form name="frmLocation" id="frmLocation" method="post" action="<?php echo $actionURL; ?>">
                    <input type="hidden" name="id" value="<?php echo ($location->id) ? $location->id : ''; ?>" id="id">
                    <!-- Location name -->
                    <div class="form-group">
                        <label>Location Name*</label>
                        <input type="text" class="form-control" name="name" value="<?php echo ($location->name) ? $location->name : ''; ?>" id="value" placeholder="Enter Location Name.">
                    </div>
                    <!-- Location price -->
                    <div class="form-group">
                        <label>Location Price*</label>
                        <input type="text" class="form-control" name="price" value="<?php echo ($location->price) ? $location->price : ''; ?>" id="value" placeholder="Enter Location Price.(INR)">
                    </div>
                    <!-- Select areas -->
                    <div>
                        <label for="groups">Select Areas: </label>
                        <select multiple="multiple" class="form-control js-example-basic-multiple" id="areaSelect" name="area_id[]">
                            <?php
                            $HiddenAreas = explode(',', rtrim($location->area_ids, ','));
                            foreach ($areas as $area) {
                                $varSelect = "";
                                if (!empty(set_value('area_id'))) {
                                    if (in_array($area->id, set_value('area_id')))
                                        $varSelect = "selected";
                                } else {
                                    if (in_array($area->id, $HiddenAreas))
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $area->id . "'>" . $area->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <!-- Select cities -->
                    <div>
                        <label for="groups">Select Cities: </label>
                        <select multiple="multiple" class="form-control js-example-basic-multiple" id="citySelect" name="city_id[]">
                            <?php
                            $HiddenCities = explode(',', rtrim($location->city_ids, ','));
                            foreach ($cities as $city) {
                                $varSelect = "";
                                if (!empty(set_value('city_id'))) {
                                    if (in_array($city->id, set_value('city_id')))
                                        $varSelect = "selected";
                                } else {
                                    if (in_array($city->id, $HiddenCities))
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $city->id . "'>" . $city->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <!-- Select states -->
                    <div>
                        <label for="groups">Select States: </label>
                        <select multiple="multiple" class="form-control js-example-basic-multiple" id="stateSelect" name="state_id[]">
                            <?php
                            $HiddenStates = explode(',', rtrim($location->state_ids, ','));
                            foreach ($states as $state) {
                                $varSelect = "";
                                if (!empty(set_value('state_id'))) {
                                    if (in_array($state->id, set_value('state_id')))
                                        $varSelect = "selected";
                                } else {
                                    if (in_array($state->id, $HiddenStates))
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $state->id . "'>" . $state->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <!-- no_time -->
                    <div class="form-group">
                        <label>No time restriction&nbsp;&nbsp;</label>
                        <input type="checkbox" class="minimal-red" <?php echo ($location->no_time) ? 'checked' : ''; ?> name="no_time" id="no_time" onchange="javascript:timepicker_block(this.checked);" value="1" >
                    </div>
                    <!-- Location time -->
                    <div class="form-group timeInfo" style="<?php echo ($location->no_time) ? 'display:none' : '' ?>">
                        <label class="timepicker_start">Playtime Restrict From</label>
                        <input type="text" class="form-control" name="start_time" value="<?php echo ($location->start_time) ? $location->start_time : ''; ?>" id="value" placeholder="Playtime Allowed From (hh:ii)">
                    </div>
                    <div class="form-group timeInfo" style="<?php echo ($location->no_time) ? 'display:none' : '' ?>">
                        <label for="timepicker_end">Playtime Restrict To</label>
                        <input type="text" class="form-control" name="end_time" value="<?php echo ($location->end_time) ? $location->end_time : ''; ?>" id="value" placeholder="Playtime Allowed To (hh:ii)">
                    </div>
            </div>
            <!-- Submit & Cancel button -->
            <div class="box-footer">
                <button type="button" name="reset" onclick="javascript:back();" class="btn btn-default">Back</button>
                <button type="submit" name="submit" onclick="javascript:submitForm();" class="btn btn-info pull-right">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<script src="<?php echo base_url('assets/select2/select2.min.js'); ?>"></script>
<script>
//select2 for user selection
                    $(".js-example-basic-multiple").select2();
//submitting form on button click.
                    function back() {
                        window.location.href = '<?php echo site_url('admin/locations'); ?>';
                    }
//submitting form on button click.
                    function submitForm() {
                        $("#frmLocation").submit();
                    }
                    //if postbag is generic show/hide receipient info
                    function timepicker_block(checkGeneric) {
                        if (checkGeneric) {
                            $('.timeInfo').hide();
                        } else {
                            $('.timeInfo').show();
                        }
                    }
</script>