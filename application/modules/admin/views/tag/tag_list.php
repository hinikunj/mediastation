<?php
//postbag creation / updation messages
if (!empty($this->session->flashdata('success'))) {
    echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
} else if (!empty($this->session->flashdata('error'))) {
    echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
}
?>
<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
        <table id="tag_table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="nosort">Sr.No</th>
                    <!-- <th>Tag ID</th> -->
                    <th>Tag Name</th>
                    <th class="nosort">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($tags as $tag) {
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <!-- <td><?php #echo $tag->id;  ?></td> -->
                        <td><?php echo $tag->name; ?></td>
                        <td>
                            <a href="<?php echo site_url('admin/tags/edit') . '/' . $tag->id; ?>" class="fa fa-edit" title="Edit"></a>
                            <a href="<?php echo site_url('admin/tags/delete') . '/' . $tag->id; ?>" onclick="return confirm('Are you sure? It will delete tag!')" class="fa fa-remove" title="Delete"></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<script>
    $(function () {
        $("#tag_table").DataTable({
            'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
        });
    });
</script>