<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Channelsvideo extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Channelsvideo_model', 'channelsvideo');
    }

    public function index() {
        $this->mTitle = 'Channelsvideo';
        // get list of users
        $this->mViewData['channelsvideos'] = $this->channelsvideo->get_many_by(array('is_delete' => 0));
        $this->render('channelsvideo/channelsvideo_list');
    }

    // Create New Channelsvideo
    public function create() {
        $this->form_validation->set_rules('channel_id', 'Channel id', 'required');
        $this->form_validation->set_rules('video_id[]', 'Video id', 'required');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            #$channel_ids = implode(',', $this->input->post('channel_id', TRUE));
            $video_ids = implode(',', $this->input->post('video_id', TRUE));
            $insertChannelsvideo_Array = array(
                'video_repeat_count' => $this->input->post('video_repeat_count', TRUE),
                'time' => $this->input->post('time', TRUE),
                'channel_id' => $this->input->post('channel_id', TRUE),
                'video_ids' => $video_ids,
                'created_by' => $this->session->userdata('user_id'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            // proceed to create Channelsvideo
            $channelsvideo_id = $this->channelsvideo->insert($insertChannelsvideo_Array);
            if ($channelsvideo_id) {
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " created channelsvideo ID:" . $channelsvideo_id,
                    'link' => site_url('admin/channelsvideo'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                // success
                $messages = "Channelsvideo successfully created.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/channelsvideo');
        }
        /* dynamic channels */
        $this->load->model('Channels_model', 'channels');
        $this->mViewData['channels'] = $this->channels->select('id,name,size_id')->get_many_by(array('is_delete' => 0));
        /* dynamic channels ends */
        /* dynamic videos */
        $this->load->model('Videos_model', 'videos');
        $this->mViewData['videos'] = $this->videos->select('id,name')->select('id,name')->get_many_by(array('is_delete' => 0));
        /* dynamic videos ends */
        $this->mViewData['actionURL'] = site_url('admin/channelsvideo/create');
        $this->mTitle = 'Create Channels video';
        $this->render('channelsvideo/create');
    }

    // Edit Channelsvideo
    public function edit($channelsvideo_id = 0) {
        $this->form_validation->set_rules('channel_id', 'Channel id', 'required');
        $this->form_validation->set_rules('video_id[]', 'Video id', 'required');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            #$channel_ids = implode(',', $this->input->post('channel_id', TRUE));
            $video_ids = implode(',', $this->input->post('video_id', TRUE));
            $updateChannelsvideoArray = array(
                'video_repeat_count' => $this->input->post('video_repeat_count', TRUE),
                'time' => $this->input->post('time', TRUE),
                'channel_id' => $this->input->post('channel_id', TRUE),
                'video_ids' => $video_ids,
                'updated_by' => $this->session->userdata('user_id'),
                'updatedtime' => date('Y-m-d H:i:s')
            );
            // proceed to update channelsvideo
            $updateFlag = $this->channelsvideo->update($channelsvideo_id, $updateChannelsvideoArray);
            if ($updateFlag) {
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " edited channelsvideo ID:" . $channelsvideo_id,
                    'link' => site_url('admin/channelsvideo'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                // success
                $messages = "Channelsvideo successfully updated.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/channelsvideo');
        }

        if ($channelsvideo_id != 0) {
            // get list of channelsvideo
            $this->mViewData['channelsvideo'] = $this->channelsvideo->get($channelsvideo_id);
            $this->mTitle = 'Edit Channelsvideo';
            /* dynamic channels */
            $this->load->model('Channels_model', 'channels');
            $this->mViewData['channels'] = $this->channels->select('id,name,size_id')->get_many_by(array('is_delete' => 0));
            /* dynamic channels ends */
            /* dynamic videos */
            $this->load->model('Videos_model', 'videos');
            $this->mViewData['videos'] = $this->videos->select('id,name')->get_many_by(array('is_delete' => 0));
            /* dynamic videos ends */
            $this->mViewData['actionURL'] = site_url('admin/channelsvideo/edit') . '/' . $channelsvideo_id;
            $this->render('channelsvideo/create');
        } else {
            redirect('admin/channelsvideo');
        }
    }

    // Soft Delete channelsvideo
    public function delete($channelsvideo_id = 0) {
        // only top-level users can reset user passwords
        $this->verify_auth(array('webmaster', 'admin'));
        // proceed to change user password
        if ($this->channelsvideo->update($channelsvideo_id, array('is_delete' => 1, 'updated_by' => $this->session->userdata('user_id')))) {
            /* logging activity */
            $activity = array(
                'user_id' => $this->session->userdata('user_id'),
                'activity' => $this->session->userdata('username') . " deleted channelsvideo ID:" . $channelsvideo_id,
                'link' => site_url('admin/channelsvideo'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            $this->ion_auth->activity_log($activity);
            /* logging activity */
            // success
            $messages = "Channelsvideo successfully deleted.";
            $this->session->set_flashdata('success', $messages);
        } else {
            // failed
            $errors = "Something went wrong. Please retry.";
            $this->session->set_flashdata('error', $errors);
        }
        redirect('admin/channelsvideo');
    }
    
    function get_sizes() {
        if (!empty($this->input->post('size_id'))) {
            $this->load->model('Videos_model', 'videos');
            $videosArr = $this->videos->select('id,name')->get_many_by(array('is_delete' => 0, 'size_id' => $this->input->post('size_id',true)));
            if (count($videosArr) > 0) {
                foreach ($videosArr as $video) {
                    $html .= '<option data-id="' . $video->name . '" value="' . $video->id . '">' . $video->name . '</option>';
                }
                echo $html;
            } else {
                echo '';
            }
        }
    }
}
