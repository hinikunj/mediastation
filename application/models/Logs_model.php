<?php

class Logs_model extends MY_Model {

    //Fetch logs for table view
    public function fetch_logs($count = false, $all = true, $limit = 5, $start = 0, $search_term = '', $sort_by = '0', $order_by = 'asc', $is_guest = false) {

        $sort_by_array = array(
            '0' => 'id',
            '1' => 'activity',
            '2' => 'link',
        );

        if ($count == false) {
            $this->db->select('id,activity,link,is_read,createdtime', false);
            $this->db->limit($limit, $start);
        } else {
            $this->db->select('COUNT(activity_log.id) AS count');
        }

        $this->db->from('activity_log');
        if ($all != false && $search_term != '') {
            $this->db->like('activity', $search_term, 'both');
        }
        
        $array = array(
            'activity_log.is_read' => 0,
        );
        $this->db->where($array);

        if ($count == false) {
            if (array_key_exists($sort_by, $sort_by_array)) {
                $this->db->order_by($sort_by_array[$sort_by], $order_by);
            }
        }
        
        $records = $this->db->get();
        return $records;
    }
    
    public function markRead() {
        $this->db->where('is_read', 0);
        $this->db->update('activity_log', array('is_read' => 1));
        return $this->db->affected_rows();
    }

}
