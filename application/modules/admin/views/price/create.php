<?php
if (!empty(validation_errors())) {
    echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
    $price->price = set_value('price');
    $price->device_id = set_value('device_id');
}
?>
<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css'); ?>">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?php echo base_url('assets/datepicker/bootstrap-datepicker.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/datepicker/bootstrap-timepicker.min.css'); ?>">
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Price Info</h3>
            </div>
            <div class="box-body">
                <form name="frmPrice" id="frmPrice" enc method="post" action="<?php echo $actionURL; ?>">
                    <input type="hidden" name="id" value="<?php echo ($price->id) ? $price->id : ''; ?>" id="id">
                    <!-- Select device -->
                    <div>
                        <label for="groups">Select Device: </label>
                        <select class="form-control js-example-basic-single" id="deviceSelect" name="device_id">
                            <option value="">--Select Device--</option>
                            <?php
                            $HiddenDevices = explode(',', rtrim($price->device_id, ','));
                            foreach ($devices as $device) {
                                $varSelect = "";
                                if (!empty(set_value('device_id'))) {
                                    if (in_array($device->id, set_value('device_id')))
                                        $varSelect = "selected";
                                } else {
                                    if (in_array($device->id, $HiddenDevices))
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $device->id . "'>" . $device->device_id . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div>&nbsp;</div>
                    <!-- Date -->
                    <div class="form-group datepicker">
                        <label>Date:</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" name="date" value="<?php echo ($price->date) ? $price->date : ''; ?>" id="datepicker" placeholder="Select date">
                        </div>
                        <!-- /.input group -->
                    </div>
                    <!-- time Picker -->
                    <div class="form-group">
                        <label>From time:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" class="form-control timepicker" name="from_time" value="<?php echo ($price->from_time) ? $price->from_time : ''; ?>" id="from_time" placeholder="From time (hh:ii)">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>To time:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" class="form-control timepicker" name="to_time" value="<?php echo ($price->to_time) ? $price->to_time : ''; ?>" id="to_time" placeholder="To time (hh:ii)">
                        </div>
                        <!-- /.input group -->
                    </div>
                    <div class="form-group timeInfo">
                        <label for="price">Price</label>
                        <input type="text" class="form-control" name="price" value="<?php echo ($price->price) ? $price->price : ''; ?>" id="price" placeholder="Price (per min)">
                    </div>
                    <!-- Submit & Cancel button -->
                    <div class="box-footer">
                        <button type="button" name="reset" onclick="javascript:back();" class="btn btn-default">Back</button>
                        <button type="submit" name="submit" onclick="javascript:submitForm();" class="btn btn-info pull-right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/select2/select2.min.js'); ?>"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url('assets/datepicker/bootstrap-datepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/datepicker/bootstrap-timepicker.min.js'); ?>"></script>
<script>
//select2 for user selection
    $(".js-example-basic-single").select2();
//submitting form on button click.
    function back() {
        window.price.href = '<?php echo site_url('admin/prices'); ?>';
    }
//submitting form on button click.
    function submitForm() {
        $("#frmPrice").submit();
    }
//Date picker
    $('#datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    })
//Timepicker
    $('.timepicker').timepicker({
        showInputs: false,
        showMeridian: false
    })
</script>