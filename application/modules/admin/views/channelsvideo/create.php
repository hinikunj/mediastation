<?php
if (!empty(validation_errors())) {
    echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
    $channelsvideo->channel_id = set_value('channel_id');
    $channelsvideo->time = set_value('time');
    $channelsvideo->video_repeat_count = set_value('video_repeat_count');
    $channelsvideo->video_ids = set_value('video_id');
}
?>
<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css'); ?>">
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Channels video Info</h3>
            </div>
            <div class="box-body">
                <form name="frmChannelsvideo" id="frmChannelsvideo" method="post" action="<?php echo $actionURL; ?>">
                    <input type="hidden" name="id" value="<?php echo ($channelsvideo->id) ? $channelsvideo->id : ''; ?>" id="id">
                    <!-- Select channels -->
                    <div>
                        <label for="groups">Select Channel: </label>
                        <select class="form-control js-example-basic-single" id="channelSelect" name="channel_id">
                            <option value="">--Select Channel--</option>>
                            <?php
                            foreach ($channels as $channel) {
                                $varSelect = "";
                                if (!empty(set_value('channel_id'))) {
                                    if ($channel->id == set_value('channel_id'))
                                        $varSelect = "selected";
                                } else {
                                    if ($channel->id == $channelsvideo->channel_id)
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect data-id='".$channel->size_id."' value='" . $channel->id . "'>" . $channel->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group"></div>
                    <!-- Select videos -->
                    <div>
                        <label for="groups">Select Videos: </label>
                        <select multiple="multiple" class="form-control js-example-basic-multiple" id="videoSelect" name="video_id[]">
                            <?php
                            $HiddenVideos = explode(',', rtrim($channelsvideo->video_ids, ','));
                            foreach ($videos as $video) {
                                $varSelect = "";
                                if (!empty(set_value('video_id'))) {
                                    if (in_array($video->id, set_value('video_id')))
                                        $varSelect = "selected";
                                } else {
                                    if (in_array($video->id, $HiddenVideos))
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $video->id . "'>" . $video->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group"></div>
                    <!-- video repeat count  -->
                    <div class="form-group">
                        <label>Video repeat count</label>
                        <input type="text" class="form-control" name="video_repeat_count" value="<?php echo ($channelsvideo->video_repeat_count) ? $channelsvideo->video_repeat_count : ''; ?>" id="video_repeat_count" placeholder="Enter video repeat count.">
                    </div>
                    <!-- time slot  -->
                    <div class="form-group">
                        <label>Time slot</label>
                        <input type="text" class="form-control" name="time" value="<?php echo ($channelsvideo->time) ? $channelsvideo->time : ''; ?>" id="time" placeholder="Enter time slot.">
                    </div>
            </div>
            <!-- Submit & Cancel button -->
            <div class="box-footer">
                <button type="button" name="reset" onclick="javascript:back();" class="btn btn-default">Back</button>
                <button type="submit" name="submit" onclick="javascript:submitForm();" class="btn btn-info pull-right">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<script src="<?php echo base_url('assets/select2/select2.min.js'); ?>"></script>
<script>
//select2 for user selection
    $(".js-example-basic-multiple").select2();
    $(".js-example-basic-single").select2();
    /*Video list on the basis of Size*/
    $('#channelSelect').on('change', function () {
        var sizeID = $(this).find(':selected').attr('data-id');
        if (sizeID) {
            $.ajax({
                type: 'POST',
                url: site_url_path + "admin/channelsvideo/get_sizes",
                data: 'size_id=' + sizeID,
                success: function (html) {
                    $('#videoSelect').html(html);
                }
            });
        } else {
            $('#videoSelect').html('');
        }
    });
//submitting form on button click.
    function back() {
        window.location.href = '<?php echo site_url('admin/channelsvideo'); ?>';
    }
//submitting form on button click.
    function submitForm() {
        $("#frmChannelsvideo").submit();
    }
</script>