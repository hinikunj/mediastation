<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Prices extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Prices_model', 'prices');
    }

    public function index() {
        $this->mTitle = 'Prices';
        // get list of users
        $this->mViewData['prices'] = $this->prices->get_many_by(array('is_delete' => 0));
        $this->render('price/price_list');
    }

    // Create New Price
    public function create() {
        $this->form_validation->set_rules('device_id', 'Device', 'required');
        $this->form_validation->set_rules('price', 'Price', 'trim|integer');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $insertPrice_Array = array(
                'device_id' => $this->input->post('device_id', TRUE),
                'from_time' => $this->input->post('from_time', TRUE),
                'to_time' => $this->input->post('to_time', TRUE),
                'price' => $this->input->post('price', TRUE),
                'date' => $this->input->post('date', TRUE),
                'created_by' => $this->session->userdata('user_id'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            // proceed to create Price
            $price_id = $this->prices->insert($insertPrice_Array);
            if ($price_id) {
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " created price:" . $this->input->post('price', TRUE) . " ID:" . $price_id,
                    'link' => site_url('admin/prices'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */

                // success
                $messages = "Price successfully created.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/prices');
        }
        /* dynamic devices*/
        $this->load->model('Devices_model', 'devices');
        $this->mViewData['devices'] = $this->devices->get_many_by(array('is_delete' => 0));
        $this->mViewData['actionURL'] = site_url('admin/prices/create');
        $this->mTitle = 'Create Price';
        $this->render('price/create');
    }

    // Edit Price
    public function edit($price_id = 0) {
        $this->form_validation->set_rules('device_id', 'Device', 'required');
        $this->form_validation->set_rules('price', 'Price', 'trim|integer');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $updatePriceArray = array(
                'device_id' => $this->input->post('device_id', TRUE),
                'from_time' => $this->input->post('from_time', TRUE),
                'to_time' => $this->input->post('to_time', TRUE),
                'price' => $this->input->post('price', TRUE),
                'date' => $this->input->post('date', TRUE),
                'updated_by' => $this->session->userdata('user_id'),
                'updatedtime' => date('Y-m-d H:i:s')
            );
            // proceed to update price
            $updateFlag = $this->prices->update($price_id, $updatePriceArray);
            if ($updateFlag) {
                // success
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " edited price:" . $this->input->post('price', TRUE) . " ID:" . $price_id,
                    'link' => site_url('admin/prices'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                $messages = "Price successfully updated.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/prices');
        }

        if ($price_id != 0) {
            // get list of price
            $this->mViewData['price'] = $this->prices->get($price_id);
            $this->mTitle = 'Edit Price';
            /* dynamic devices*/
            $this->load->model('Devices_model', 'devices');
            $this->mViewData['devices'] = $this->devices->get_many_by(array('is_delete' => 0));
            $this->mViewData['actionURL'] = site_url('admin/prices/edit') . '/' . $price_id;
            $this->render('price/create');
        } else {
            redirect('admin/prices');
        }
    }

    // Soft Delete price
    public function delete($price_id = 0) {
        // only top-level users can reset user passwords
        $this->verify_auth(array('webmaster', 'admin'));
        // proceed to change user password
        if ($this->prices->update($price_id, array('is_delete' => 1, 'updated_by' => $this->session->userdata('user_id')))) {
            /* logging activity */
            $activity = array(
                'user_id' => $this->session->userdata('user_id'),
                'activity' => $this->session->userdata('username') . " deleted price ID:" . $price_id,
                'link' => site_url('admin/prices'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            $this->ion_auth->activity_log($activity);
            /* logging activity */
            // success
            $messages = "Price successfully deleted.";
            $this->session->set_flashdata('success', $messages);
        } else {
            // failed
            $errors = "Something went wrong. Please retry.";
            $this->session->set_flashdata('error', $errors);
        }
        redirect('admin/prices');
    }

}
