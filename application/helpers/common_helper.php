<?php

/**
* This function is for curl call.
* 
* @return string
* @author Nikunj Dhimar
**/
function curl_post($addr, $postArray) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $addr);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postArray);
    // Download the given URL, and return output
    $output = curl_exec($ch) or die(curl_error($ch));
    // Close the cURL resource, and free system resources
    curl_close($ch);
    if ($output === false) {
        
    }
    return $output;
}

/**
* This function is for curl call.
* 
* Method: POST, PUT, GET etc
* Data: array("param" => "value") ==> index.php?param=value
* @return string
* @author Nikunj Dhimar
**/
function CallAPI($method, $url, $data = false) {
    $curl = curl_init();
    switch ($method) {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);
            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }
    // Content-Type Json:
    /*curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json'
    ));*/
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);
    return $result;
}

/**
* This function will generate time elapsed.
* 
* @return string
* @author Nikunj Dhimar
**/
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full)
        $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

/**
* This function use to fetch the user browser information.
* 
* @return string
* @author Nikunj Dhimar
**/
function Agent() {
    $ci = & get_instance();
    if ($ci->agent->is_browser()) {
        $agent = $ci->agent->browser() . ' ' . $ci->agent->version();
    } elseif ($ci->agent->is_robot()) {
        $agent = $ci->agent->robot();
    } elseif ($ci->agent->is_mobile()) {
        $agent = $ci->agent->mobile();
    } else {
        $agent = 'Unidentified User Agent';
    }
    $platform = $ci->agent->platform();

    return $platform . " " . $agent;
}

/**
* This function will convert object into array.
* 
* @return array
* @author Nikunj Dhimar
**/
function objectToArray($obj) {
    $arr = array();
    foreach ($obj as $key => $val)
        $arr[$key] = (is_array($val) || is_object($val)) ? objectToArray($val) : $val;
    return $arr;
}

/**
* This function returns error message and code.
* 
* @return array
* @author Nikunj Dhimar
**/
function get_response_format($status = FALSE, $error_code = 1, $status_code = 400, $additional_param = array(), $form_validation_errors = false) {
    $message = array();
    $message['status'] = $status;
    $message['error_code'] = $error_code;
    $message['code'] = $status_code;
    if (!empty($additional_param)) {
        foreach ($additional_param as $key => $val) {
            $message[$key] = $val;
        }
    }
    if ($form_validation_errors) {
        $message['validation_errors'] = $form_validation_errors;
    }
    return $message;
}
