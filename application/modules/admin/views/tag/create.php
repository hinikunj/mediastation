<?php
if (!empty(validation_errors())) {
    echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
    $tag->name = set_value('name');
}
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Tag Info</h3>
            </div>
            <div class="box-body">
                <form name="frmTag" id="frmTag" method="post" action="<?php echo $actionURL; ?>">
                    <input type="hidden" name="id" value="<?php echo ($tag->id) ? $tag->id : ''; ?>" id="id">
                    <!-- Tag name -->
                    <div class="form-group">
                        <label>Tag Name*</label>
                        <input type="text" class="form-control" name="name" value="<?php echo ($tag->name) ? $tag->name : ''; ?>" id="value" placeholder="Enter Tag Name.">
                    </div>
                    <!-- Submit & Cancel button -->
                    <div class="box-footer">
                        <button type="button" name="reset" onclick="javascript:back();" class="btn btn-default">Back</button>
                        <button type="submit" name="submit" onclick="javascript:submitForm();" class="btn btn-info pull-right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
//submitting form on button click.
    function back() {
        window.location.href = '<?php echo site_url('admin/tags'); ?>';
    }
//submitting form on button click.
    function submitForm() {
        $("#frmTag").submit();
    }
</script>