<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sizes extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Sizes_model', 'sizes');
        $this->load->helper('security');
    }

    public function index() {
        $this->mTitle = 'Sizes';
        // get list of users
        $this->mViewData['sizes'] = $this->sizes->get_many_by(array('is_delete' => 0));
        $this->render('size/size_list');
    }

    // Create New Size
    public function create() {
        $this->form_validation->set_rules('name', 'Size name', 'required|callback_unique_size_check');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $insertSize_Array = array(
                'name' => $this->input->post('name', TRUE),
                'created_by' => $this->session->userdata('user_id'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            // proceed to create Size
            $size_id = $this->sizes->insert($insertSize_Array);
            if ($size_id) {
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " created size Name:" . $this->input->post('name', TRUE) . " ID:" . $size_id,
                    'link' => site_url('admin/sizes'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                /*creating folder in uploads with each new size*/
                if (!is_dir('uploads/videos/' . $size_id)) {
                    mkdir('./uploads/videos/' . $size_id, 0777, TRUE);
                }
                // success
                $messages = "Size successfully created.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/sizes');
        }
        $this->mViewData['actionURL'] = site_url('admin/sizes/create');
        $this->mTitle = 'Create Size';
        $this->render('size/create');
    }

    // Edit Size
    public function edit($size_id = 0) {
        $this->form_validation->set_rules('name', 'Size name', 'required|callback_unique_size_check');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $updateSizeArray = array(
                'name' => $this->input->post('name', TRUE),
                'updated_by' => $this->session->userdata('user_id'),
                'updatedtime' => date('Y-m-d H:i:s')
            );
            // proceed to update size
            $updateFlag = $this->sizes->update($size_id, $updateSizeArray);
            if ($updateFlag) {
                // success
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " edited size Name:" . $this->input->post('name', TRUE) . " ID:" . $size_id,
                    'link' => site_url('admin/sizes'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                $messages = "Size successfully updated.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/sizes');
        }

        if ($size_id != 0) {
            // get list of size
            $this->mViewData['size'] = $this->sizes->get($size_id);
            $this->mTitle = 'Edit Size';
            $this->mViewData['actionURL'] = site_url('admin/sizes/edit') . '/' . $size_id;
            $this->render('size/create');
        } else {
            redirect('admin/sizes');
        }
    }

    // Soft Delete size
    public function delete($size_id = 0) {
        // only top-level users can reset user passwords
        $this->verify_auth(array('webmaster', 'admin'));
        // proceed to change user password
        if ($this->sizes->update($size_id, array('is_delete' => 1, 'updated_by' => $this->session->userdata('user_id')))) {
            /* logging activity */
            $activity = array(
                'user_id' => $this->session->userdata('user_id'),
                'activity' => $this->session->userdata('username') . " deleted size ID:" . $size_id,
                'link' => site_url('admin/sizes'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            $this->ion_auth->activity_log($activity);
            /* logging activity */
            // success
            $messages = "Size successfully deleted.";
            $this->session->set_flashdata('success', $messages);
        } else {
            // failed
            $errors = "Something went wrong. Please retry.";
            $this->session->set_flashdata('error', $errors);
        }
        redirect('admin/sizes');
    }

    //callback for unique size name
    public function unique_size_check($str) {
        if ($this->input->post('id', TRUE))
            $this->db->where_not_in('id', $this->input->post('id', TRUE));
        $this->db->where('name', $str);
        if ($this->db->count_all_results('sizes') > 0) {
            $this->form_validation->set_message('unique_size_check', 'The {field} field must contain a unique value.');
            return FALSE;
        } else {
            return true;
        }
    }

}
