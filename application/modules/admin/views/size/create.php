<?php
if (!empty(validation_errors())) {
    echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
    $size->name = set_value('name');
}
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Size Info</h3>
            </div>
            <div class="box-body">
                <form name="frmSize" id="frmSize" method="post" action="<?php echo $actionURL; ?>">
                    <input type="hidden" name="id" value="<?php echo ($size->id) ? $size->id : ''; ?>" id="id">
                    <!-- Size name -->
                    <div class="form-group">
                        <label>Size Name*</label>
                        <input type="text" class="form-control" name="name" value="<?php echo ($size->name) ? $size->name : ''; ?>" id="value" placeholder="Enter Size Name.">
                    </div>
                    <!-- Submit & Cancel button -->
                    <div class="box-footer">
                        <button type="button" name="reset" onclick="javascript:back();" class="btn btn-default">Back</button>
                        <button type="submit" name="submit" onclick="javascript:submitForm();" class="btn btn-info pull-right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
//submitting form on button click.
    function back() {
        window.location.href = '<?php echo site_url('admin/sizes'); ?>';
    }
//submitting form on button click.
    function submitForm() {
        $("#frmSize").submit();
    }
</script>