<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {

	public function index()
	{
		$this->load->model('admin_user_model', 'm_admin');
		$this->mViewData['count'] = array(
			'logs' => $this->m_admin->countActivity(),
		);
		$this->render('home');
	}
}
