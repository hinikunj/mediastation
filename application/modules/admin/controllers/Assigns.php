<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Assigns extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Assigns_model', 'assigns');
    }

    public function index() {
        $this->mTitle = 'Assigns';
        // get list of users
        $this->mViewData['assigns'] = $this->assigns->get_many_by(array('is_delete' => 0));
        $this->render('assign/assign_list');
    }

    // Create New Assign
    public function create() {
        $this->form_validation->set_rules('device_id', 'Device', 'required');
        $this->form_validation->set_rules('channel_id', 'Channel', 'required');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $insertAssign_Array = array(
                'device_id' => $this->input->post('device_id', TRUE),
                'channel_id' => $this->input->post('channel_id', TRUE),
                'channel_repeat_count' => $this->input->post('channel_repeat_count', TRUE),
                'time' => $this->input->post('time', TRUE),
                'created_by' => $this->session->userdata('user_id'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            // proceed to create Assign
            $assign_id = $this->assigns->insert($insertAssign_Array);
            if ($assign_id) {
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " created assign ID:" . $this->input->post('assign_id', TRUE) . " ID:" . $assign_id,
                    'link' => site_url('admin/assigns'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */

                // success
                $messages = "Channel Assigned successfully created.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/assigns');
        }
        /* dynamic devices*/
        $this->load->model('Devices_model', 'devices');
        $this->mViewData['devices'] = $this->devices->get_many_by(array('is_delete' => 0));
        /* dynamic channels */
        $this->load->model('Channels_model', 'channels');
        $this->mViewData['channels'] = $this->channels->get_many_by(array('is_delete' => 0));
        $this->mViewData['actionURL'] = site_url('admin/assigns/create');
        $this->mTitle = 'Create Assign';
        $this->render('assign/create');
    }

    // Edit Assign
    public function edit($assign_id = 0) {
        $this->form_validation->set_rules('assign_id', 'Assign id', 'required');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $updateAssignArray = array(
                'device_id' => $this->input->post('device_id', TRUE),
                'channel_id' => $this->input->post('channel_id', TRUE),
                'channel_repeat_count' => $this->input->post('channel_repeat_count', TRUE),
                'time' => $this->input->post('time', TRUE),
                'updated_by' => $this->session->userdata('user_id'),
                'updatedtime' => date('Y-m-d H:i:s')
            );
            // proceed to update assign
            $updateFlag = $this->assigns->update($assign_id, $updateAssignArray);
            if ($updateFlag) {
                // success
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " edited assign id:" . $this->input->post('assign_id', TRUE) . " ID:" . $assign_id,
                    'link' => site_url('admin/assigns'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                $messages = "Assign successfully updated.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/assigns');
        }

        if ($assign_id != 0) {
            // get list of assign
            $this->mViewData['assign'] = $this->assigns->get($assign_id);
            $this->mTitle = 'Edit Assign';
            /* dynamic devices*/
            $this->load->model('Devices_model', 'devices');
            $this->mViewData['devices'] = $this->devices->get_many_by(array('is_delete' => 0));
            /* dynamic channels */
            $this->load->model('Channels_model', 'channels');
            $this->mViewData['channels'] = $this->channels->get_many_by(array('is_delete' => 0));
            $this->mViewData['actionURL'] = site_url('admin/assigns/edit') . '/' . $assign_id;
            $this->render('assign/create');
        } else {
            redirect('admin/assigns');
        }
    }

    // Soft Delete assign
    public function delete($assign_id = 0) {
        // only top-level users can reset user passwords
        $this->verify_auth(array('webmaster', 'admin'));
        // proceed to change user password
        if ($this->assigns->update($assign_id, array('is_delete' => 1, 'updated_by' => $this->session->userdata('user_id')))) {
            /* logging activity */
            $activity = array(
                'user_id' => $this->session->userdata('user_id'),
                'activity' => $this->session->userdata('username') . " deleted assign ID:" . $assign_id,
                'link' => site_url('admin/assigns'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            $this->ion_auth->activity_log($activity);
            /* logging activity */
            // success
            $messages = "Assign successfully deleted.";
            $this->session->set_flashdata('success', $messages);
        } else {
            // failed
            $errors = "Something went wrong. Please retry.";
            $this->session->set_flashdata('error', $errors);
        }
        redirect('admin/assigns');
    }

}
