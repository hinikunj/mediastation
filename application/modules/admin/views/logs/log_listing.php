<?php
//db operation messages
if (!empty($this->session->flashdata('success'))) {
    echo '<div class="alert alert-success flash-alert">' . $this->session->flashdata('success') . '</div>';
} else if (!empty($this->session->flashdata('error'))) {
    echo '<div class="alert alert-danger  flash-alert">' . $this->session->flashdata('error') . '</div>';
}
?>
<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
        <!-- Filters of Postbag orders -->
        <div class="row">
            <div class="col-sm-2 pull-right">
                <a href="<?php echo site_url('admin/logs/mark_read'); ?>" onclick="return confirm('Are you sure? you want to mark all as read!')">
                    <button type="button" class="btn btn-sm btn-block btn-danger">Mark all as read</button>
                </a>
            </div>
        </div>
        <div class="row">
            &nbsp;
        </div>
        <table id="logs_table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Activity</th>
                    <th class="nosort">Navigate</th>
                    <!-- <th class="nosort">Action</th> -->
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<script>
    $(function () {
        $("#logs_table").DataTable({
            "processing": true,
            "serverSide": true,
            "iDisplayLength": 10,
            "ajax": site_url_path + "/admin/logs/ajax_logs",
            "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }],
            "order": [[ 0, "desc" ]]
        });
    });
</script>