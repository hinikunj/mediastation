<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Channels extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Channels_model', 'channels');
        $this->load->helper('security');
    }

    public function index() {
        $this->mTitle = 'Channels';
        // get list of users
        $this->mViewData['channels'] = $this->channels->get_many_by(array('is_delete' => 0));
        $this->render('channel/channel_list');
    }

    // Create New Channel
    public function create() {
        $this->form_validation->set_rules('name', 'Channel name', 'required|callback_unique_channel_check');
        $this->form_validation->set_rules('time', 'Channel time', 'trim|integer');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $insertChannel_Array = array(
                'name' => $this->input->post('name', TRUE),
                'size_id' => $this->input->post('size_id', TRUE),
                'time' => $this->input->post('time', TRUE),
                'created_by' => $this->session->userdata('user_id'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            // proceed to create Channel
            $channel_id = $this->channels->insert($insertChannel_Array);
            if ($channel_id) {
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " created channel Name:" . $this->input->post('name', TRUE) . " ID:" . $channel_id,
                    'link' => site_url('admin/channels'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */

                // success
                $messages = "Channel successfully created.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/channels');
        }
        /* dynamic Video size (resolution) */
        $this->load->model('Sizes_model', 'sizes');
        $this->mViewData['sizes'] = $this->sizes->get_many_by(array('is_delete' => 0));
        /* dynamic tag | Video */
        $this->mViewData['actionURL'] = site_url('admin/channels/create');
        $this->mTitle = 'Create Channel';
        $this->render('channel/create');
    }

    // Edit Channel
    public function edit($channel_id = 0) {
        $this->form_validation->set_rules('name', 'Channel name', 'required|callback_unique_channel_check');
        $this->form_validation->set_rules('time', 'Channel time', 'trim|integer');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $updateChannelArray = array(
                'name' => $this->input->post('name', TRUE),
                'size_id' => $this->input->post('size_id', TRUE),
                'time' => $this->input->post('time', TRUE),
                'updated_by' => $this->session->userdata('user_id'),
                'updatedtime' => date('Y-m-d H:i:s')
            );
            // proceed to update channel
            $updateFlag = $this->channels->update($channel_id, $updateChannelArray);
            if ($updateFlag) {
                // success
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " edited channel Name:" . $this->input->post('name', TRUE) . " ID:" . $channel_id,
                    'link' => site_url('admin/channels'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                $messages = "Channel successfully updated.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/channels');
        }

        if ($channel_id != 0) {
            // get list of channel
            $this->mViewData['channel'] = $this->channels->get($channel_id);
            $this->mTitle = 'Edit Channel';
            /* dynamic Video size (resolution) */
            $this->load->model('Sizes_model', 'sizes');
            $this->mViewData['sizes'] = $this->sizes->get_many_by(array('is_delete' => 0));
            /* dynamic tag | Video */
            $this->mViewData['actionURL'] = site_url('admin/channels/edit') . '/' . $channel_id;
            $this->render('channel/create');
        } else {
            redirect('admin/channels');
        }
    }

    // Soft Delete channel
    public function delete($channel_id = 0) {
        // only top-level users can reset user passwords
        $this->verify_auth(array('webmaster', 'admin'));
        // proceed to change user password
        if ($this->channels->update($channel_id, array('is_delete' => 1, 'updated_by' => $this->session->userdata('user_id')))) {
            /* logging activity */
            $activity = array(
                'user_id' => $this->session->userdata('user_id'),
                'activity' => $this->session->userdata('username') . " deleted channel ID:" . $channel_id,
                'link' => site_url('admin/channels'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            $this->ion_auth->activity_log($activity);
            /* logging activity */
            // success
            $messages = "Channel successfully deleted.";
            $this->session->set_flashdata('success', $messages);
        } else {
            // failed
            $errors = "Something went wrong. Please retry.";
            $this->session->set_flashdata('error', $errors);
        }
        redirect('admin/channels');
    }

    //callback for unique channel name
    public function unique_channel_check($str) {
        if ($this->input->post('id', TRUE))
            $this->db->where_not_in('id', $this->input->post('id', TRUE));
        $this->db->where('name', $str);
        if ($this->db->count_all_results('channels') > 0) {
            $this->form_validation->set_message('unique_channel_check', 'The {field} field must contain a unique value.');
            return FALSE;
        } else {
            return true;
        }
    }

}
