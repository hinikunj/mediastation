<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Devices extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Devices_model', 'devices');
    }

    public function index() {
        $this->mTitle = 'Devices';
        // get list of users
        $this->mViewData['devices'] = $this->devices->get_many_by(array('is_delete' => 0));
        $this->render('device/device_list');
    }

    // Create New Device
    public function create() {
        $this->form_validation->set_rules('device_id', 'Device id', 'required');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $tag_ids = implode(',', $this->input->post('tag_id', TRUE));
            $start_datetime = $this->input->post('start_date', true) . ' ' . $this->input->post('start_time', true);
            $end_datetime = $this->input->post('end_date', true) . ' ' . $this->input->post('end_time', true);
            $insertDevice_Array = array(
                'location_id' => $this->input->post('location_id', TRUE),
                'size_id' => $this->input->post('size_id', TRUE),
                'number_of_screen' => $this->input->post('number_of_screen', TRUE),
                'tag_ids' => $tag_ids,
                'device_id' => $this->input->post('device_id', TRUE),
                'start_datetime' => $start_datetime,
                'end_datetime' => $end_datetime,
                'created_by' => $this->session->userdata('user_id'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            // proceed to create Device
            $device_id = $this->devices->insert($insertDevice_Array);
            if ($device_id) {
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " created device ID:" . $this->input->post('device_id', TRUE) . " ID:" . $device_id,
                    'link' => site_url('admin/devices'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */

                // success
                $messages = "Device successfully created.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/devices');
        }
        /* dynamic Video size (resolution) */
        $this->load->model('Sizes_model', 'sizes');
        $this->mViewData['sizes'] = $this->sizes->get_many_by(array('is_delete' => 0));
        /* dynamic tag | Video */
        $this->load->model('Tags_model', 'tags');
        $this->mViewData['tags'] = $this->tags->get_many_by(array('is_delete' => 0));
        /* dynamic tag ends */
        /* dynamic locations */
        $this->load->model('Locations_model', 'locations');
        $this->mViewData['locations'] = $this->locations->get_many_by(array('is_delete' => 0));
        /* dynamic locations ends */
        $this->mViewData['actionURL'] = site_url('admin/devices/create');
        $this->mTitle = 'Create Device';
        $this->render('device/create');
    }

    // Edit Device
    public function edit($device_id = 0) {
        $this->form_validation->set_rules('device_id', 'Device id', 'required');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $tag_ids = implode(',', $this->input->post('tag_id', TRUE));
            $start_datetime = $this->input->post('start_date', true) . ' ' . $this->input->post('start_time', true);
            $end_datetime = $this->input->post('end_date', true) . ' ' . $this->input->post('end_time', true);
            $updateDeviceArray = array(
                'location_id' => $this->input->post('location_id', TRUE),
                'size_id' => $this->input->post('size_id', TRUE),
                'number_of_screen' => $this->input->post('number_of_screen', TRUE),
                'tag_ids' => $tag_ids,
                'device_id' => $this->input->post('device_id', TRUE),
                'start_datetime' => $start_datetime,
                'end_datetime' => $end_datetime,
                'updated_by' => $this->session->userdata('user_id'),
                'updatedtime' => date('Y-m-d H:i:s')
            );
            // proceed to update device
            $updateFlag = $this->devices->update($device_id, $updateDeviceArray);
            if ($updateFlag) {
                // success
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " edited device id:" . $this->input->post('device_id', TRUE) . " ID:" . $device_id,
                    'link' => site_url('admin/devices'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                $messages = "Device successfully updated.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/devices');
        }

        if ($device_id != 0) {
            // get list of device
            $this->mViewData['device'] = $this->devices->get($device_id);
            $this->mTitle = 'Edit Device';
            /* dynamic Video size (resolution) */
            $this->load->model('Sizes_model', 'sizes');
            $this->mViewData['sizes'] = $this->sizes->get_many_by(array('is_delete' => 0));
            /* dynamic tag | Video */
            $this->load->model('Tags_model', 'tags');
            $this->mViewData['tags'] = $this->tags->get_many_by(array('is_delete' => 0));
            /* dynamic tag ends */
            /* dynamic locations | Video */
            $this->load->model('Locations_model', 'locations');
            $this->mViewData['locations'] = $this->locations->get_many_by(array('is_delete' => 0));
            /* dynamic locations ends */
            $this->mViewData['actionURL'] = site_url('admin/devices/edit') . '/' . $device_id;
            $this->render('device/create');
        } else {
            redirect('admin/devices');
        }
    }

    // Soft Delete device
    public function delete($device_id = 0) {
        // only top-level users can reset user passwords
        $this->verify_auth(array('webmaster', 'admin'));
        // proceed to change user password
        if ($this->devices->update($device_id, array('is_delete' => 1, 'updated_by' => $this->session->userdata('user_id')))) {
            /* logging activity */
            $activity = array(
                'user_id' => $this->session->userdata('user_id'),
                'activity' => $this->session->userdata('username') . " deleted device ID:" . $device_id,
                'link' => site_url('admin/devices'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            $this->ion_auth->activity_log($activity);
            /* logging activity */
            // success
            $messages = "Device successfully deleted.";
            $this->session->set_flashdata('success', $messages);
        } else {
            // failed
            $errors = "Something went wrong. Please retry.";
            $this->session->set_flashdata('error', $errors);
        }
        redirect('admin/devices');
    }

}
