<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cities extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Cities_model', 'cities');
        $this->load->helper('security');
    }

    public function index() {
        $this->mTitle = 'Cities';
        // get list of users
        $this->mViewData['cities'] = $this->cities->get_many_by(array('is_delete' => 0));
        $this->render('city/city_list');
    }

    // Create New City
    public function create() {
        $this->form_validation->set_rules('name', 'City name', 'required|callback_unique_city_check');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $insertCity_Array = array(
                'name' => $this->input->post('name', TRUE),
                'created_by' => $this->session->userdata('user_id'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            // proceed to create City
            $city_id = $this->cities->insert($insertCity_Array);
            if ($city_id) {
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " created city Name:" . $this->input->post('name', TRUE) . " ID:" . $city_id,
                    'link' => site_url('admin/cities'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */

                // success
                $messages = "City successfully created.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/cities');
        }
        $this->mViewData['actionURL'] = site_url('admin/cities/create');
        $this->mTitle = 'Create City';
        $this->render('city/create');
    }

    // Edit City
    public function edit($city_id = 0) {
        $this->form_validation->set_rules('name', 'City name', 'required|callback_unique_city_check');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $updateCityArray = array(
                'name' => $this->input->post('name', TRUE),
                'updated_by' => $this->session->userdata('user_id'),
                'updatedtime' => date('Y-m-d H:i:s')
            );
            // proceed to update city
            $updateFlag = $this->cities->update($city_id, $updateCityArray);
            if ($updateFlag) {
                // success
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " edited city Name:" . $this->input->post('name', TRUE) . " ID:" . $city_id,
                    'link' => site_url('admin/cities'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                $messages = "City successfully updated.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/cities');
        }

        if ($city_id != 0) {
            // get list of city
            $this->mViewData['city'] = $this->cities->get($city_id);
            $this->mTitle = 'Edit City';
            $this->mViewData['actionURL'] = site_url('admin/cities/edit') . '/' . $city_id;
            $this->render('city/create');
        } else {
            redirect('admin/cities');
        }
    }

    // Soft Delete city
    public function delete($city_id = 0) {
        // only top-level users can reset user passwords
        $this->verify_auth(array('webmaster', 'admin'));
        // proceed to change user password
        if ($this->cities->update($city_id, array('is_delete' => 1, 'updated_by' => $this->session->userdata('user_id')))) {
            /* logging activity */
            $activity = array(
                'user_id' => $this->session->userdata('user_id'),
                'activity' => $this->session->userdata('username') . " deleted city ID:" . $city_id,
                'link' => site_url('admin/cities'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            $this->ion_auth->activity_log($activity);
            /* logging activity */
            // success
            $messages = "City successfully deleted.";
            $this->session->set_flashdata('success', $messages);
        } else {
            // failed
            $errors = "Something went wrong. Please retry.";
            $this->session->set_flashdata('error', $errors);
        }
        redirect('admin/cities');
    }

    //callback for unique city name
    public function unique_city_check($str) {
        if ($this->input->post('id', TRUE))
            $this->db->where_not_in('id', $this->input->post('id', TRUE));
        $this->db->where('name', $str);
        if ($this->db->count_all_results('cities') > 0) {
            $this->form_validation->set_message('unique_city_check', 'The {field} field must contain a unique value.');
            return FALSE;
        } else {
            return true;
        }
    }

}
