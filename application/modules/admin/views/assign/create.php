<?php
if (!empty(validation_errors())) {
    echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
    $assign->channel_id = set_value('channel_id');
    $assign->device_id = set_value('device_id');
    $assign->channel_repeat_count = set_value('channel_repeat_count');
    $assign->time = set_value('time');
}
?>
<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css'); ?>">
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Assign Info</h3>
            </div>
            <div class="box-body">
                <form name="frmAssign" id="frmAssign" method="post" action="<?php echo $actionURL; ?>">
                    <input type="hidden" name="id" value="<?php echo ($assign->id) ? $assign->id : ''; ?>" id="id">
                    <!-- Select device -->
                    <div>
                        <label for="groups">Select Device: </label>
                        <select class="form-control js-example-basic-single" id="deviceSelect" name="device_id">
                            <option value="">--Select Device--</option>
                            <?php
                            $HiddenDevices = explode(',', rtrim($assign->device_id, ','));
                            foreach ($devices as $device) {
                                $varSelect = "";
                                if (!empty(set_value('device_id'))) {
                                    if (in_array($device->id, set_value('device_id')))
                                        $varSelect = "selected";
                                } else {
                                    if (in_array($device->id, $HiddenDevices))
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $device->id . "'>" . $device->device_id . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div>&nbsp;</div>
                    <!-- Select Channel -->
                    <div>
                        <label for="groups">Select Channel: </label>
                        <select class="form-control js-example-basic-single" id="channelSelect" name="channel_id">
                            <option value="">--Select Channel--</option>
                            <?php
                            $HiddenChannels = explode(',', rtrim($assign->channel_id, ','));
                            foreach ($channels as $channel) {
                                $varSelect = "";
                                if (!empty(set_value('channel_id'))) {
                                    if (in_array($channel->id, set_value('channel_id')))
                                        $varSelect = "selected";
                                } else {
                                    if (in_array($channel->id, $HiddenChannels))
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $channel->id . "'>" . $channel->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div>&nbsp;</div>
                    <!-- Channel repeat count -->
                    <div class="form-group">
                        <label>Channel repeat count</label>
                        <input type="text" class="form-control" name="channel_repeat_count" value="<?php echo ($assign->channel_repeat_count) ? $assign->channel_repeat_count : ''; ?>" id="value" placeholder="Enter Channel repeat count.">
                    </div>
                    <!-- Channel name -->
                    <div class="form-group">
                        <label>Time*</label>
                        <input type="text" class="form-control" name="time" value="<?php echo ($assign->time) ? $assign->time : ''; ?>" id="value" placeholder="Enter Time (secods).">
                    </div>
                    <!-- Submit & Cancel button -->
                    <div class="box-footer">
                        <button type="button" name="reset" onclick="javascript:back();" class="btn btn-default">Back</button>
                        <button type="submit" name="submit" onclick="javascript:submitForm();" class="btn btn-info pull-right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/select2/select2.min.js'); ?>"></script>
<script>
//select2 for user selection
    $(".js-example-basic-single").select2();
//submitting form on button click.
    function back() {
        window.location.href = '<?php echo site_url('admin/assigns'); ?>';
    }
//submitting form on button click.
    function submitForm() {
        $("#frmAssign").submit();
    }
</script>