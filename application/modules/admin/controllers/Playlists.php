<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Playlists extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Playlists_model', 'playlists');
    }

    public function index() {
        $this->mTitle = 'Playlists';
        // get list of users
        $this->mViewData['playlists'] = $this->playlists->get_many_by(array('is_delete' => 0));
        $this->render('playlist/playlist_list');
    }

    // Create New Playlist
    public function create() {
        $this->form_validation->set_rules('name', 'Playlist name', 'required');
        if ($this->form_validation->run() == TRUE) {
            // passed validation
            $tag_ids = implode(',', $this->input->post('tag_id', TRUE));
            $video_ids = implode(',', $this->input->post('video_id', TRUE));
            $location_ids = implode(',', $this->input->post('location_id', TRUE));
            $insertPlaylist_Array = array(
                'name' => $this->input->post('name', TRUE),
                'start_time' => $this->input->post('start_time', TRUE),
                'end_time' => $this->input->post('end_time', TRUE),
                'tag_ids' => $tag_ids,
                'video_ids' => $video_ids,
                'location_ids' => $location_ids,
                'is_active' => 0,
                'created_by' => $this->session->userdata('user_id'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            // proceed to create Playlist
            $playlist_id = $this->playlists->insert($insertPlaylist_Array);
            if ($playlist_id) {
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " created playlist Name:" . $this->input->post('name', TRUE) . " ID:" . $playlist_id,
                    'link' => site_url('admin/playlists'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                // success
                $messages = "Playlist successfully created.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/playlists');
        }
        /* dynamic tag | Video */
        $this->load->model('Tags_model', 'tags');
        $this->mViewData['tags'] = $this->tags->get_many_by(array('is_delete' => 0));
        /* dynamic tag ends */
        $this->mViewData['actionURL'] = site_url('admin/playlists/create');
        $this->mTitle = 'Create Playlist';
        $this->render('playlist/create');
    }

    // Edit Playlist
    public function edit($playlist_id = 0) {
        $this->form_validation->set_rules('name', 'Playlist name', 'required');
        if ($this->form_validation->run() == TRUE) {
            // passed validation
            $tag_ids = implode(',', $this->input->post('tag_id', TRUE));
            $video_ids = implode(',', $this->input->post('video_id', TRUE));
            $location_ids = implode(',', $this->input->post('location_id', TRUE));
            $updatePlaylistArray = array(
                'name' => $this->input->post('name', TRUE),
                'start_time' => $this->input->post('start_time', TRUE),
                'end_time' => $this->input->post('end_time', TRUE),
                'tag_ids' => $tag_ids,
                'video_ids' => $video_ids,
                'location_ids' => $location_ids,
                'is_active' => 0,
                'updated_by' => $this->session->userdata('user_id'),
                'updatedtime' => date('Y-m-d H:i:s')
            );
            // proceed to update playlist
            $updateFlag = $this->playlists->update($playlist_id, $updatePlaylistArray);
            if ($updateFlag) {
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " edited playlist ID:" . $playlist_id,
                    'link' => site_url('admin/playlists'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                // success
                $messages = "Playlist successfully updated.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/playlists');
        }

        if ($playlist_id != 0) {
            // get list of playlist
            $this->mViewData['playlist'] = $this->playlists->get($playlist_id);
            $this->mTitle = 'Edit Playlist';
            /* dynamic tag */
            $this->load->model('Tags_model', 'tags');
            $this->mViewData['tags'] = $this->tags->get_many_by(array('is_delete' => 0));

            $this->load->model('Videos_model', 'videos');
            $this->mViewData['videos'] = $this->videos->get_many_by(array('is_delete' => 0));

            $this->load->model('Locations_model', 'locations');
            $this->mViewData['locations'] = $this->locations->get_many_by(array('is_delete' => 0));
            /* dynamic tag ends */
            $this->mViewData['actionURL'] = site_url('admin/playlists/edit') . '/' . $playlist_id;
            $this->render('playlist/create');
        } else {
            redirect('admin/playlists');
        }
    }

    // Active playlist
    public function active($playlist_id = 0) {
        // only top-level users can reset user passwords
        $this->verify_auth(array('webmaster', 'admin'));
        // proceed to change user password
        if ($this->playlists->update($playlist_id, array('is_active' => 1, 'updated_by' => $this->session->userdata('user_id')))) {
            /* logging activity */
            $activity = array(
                'user_id' => $this->session->userdata('user_id'),
                'activity' => $this->session->userdata('username') . " change playlist status ID:" . $playlist_id,
                'link' => site_url('admin/playlists'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            $this->ion_auth->activity_log($activity);
            /* logging activity */
            // success
            $messages = "Playlist successfully activated.";
            $this->session->set_flashdata('success', $messages);
        } else {
            // failed
            $errors = "Something went wrong. Please retry.";
            $this->session->set_flashdata('error', $errors);
        }
        redirect('admin/playlists');
    }

    // Soft Delete playlist
    public function delete($playlist_id = 0) {
        // only top-level users can reset user passwords
        $this->verify_auth(array('webmaster', 'admin'));
        // proceed to change user password
        if ($this->playlists->update($playlist_id, array('is_delete' => 1, 'updated_by' => $this->session->userdata('user_id')))) {
            /* logging activity */
            $activity = array(
                'user_id' => $this->session->userdata('user_id'),
                'activity' => $this->session->userdata('username') . " deleted playlist ID:" . $playlist_id,
                'link' => site_url('admin/playlists'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            $this->ion_auth->activity_log($activity);
            /* logging activity */
            // success
            $messages = "Playlist successfully deleted.";
            $this->session->set_flashdata('success', $messages);
        } else {
            // failed
            $errors = "Something went wrong. Please retry.";
            $this->session->set_flashdata('error', $errors);
        }
        redirect('admin/playlists');
    }

}
