<?php
//postbag creation / updation messages
if (!empty($this->session->flashdata('success'))) {
    echo '<div class="alert alert-success">' . $this->session->flashdata('success') . '</div>';
} else if (!empty($this->session->flashdata('error'))) {
    echo '<div class="alert alert-danger">' . $this->session->flashdata('error') . '</div>';
}
?>
<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
        <table id="assign_table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="nosort">Sr.No</th>
                     <th>Device id</th>
                     <th>Channel id</th>
                    <th class="nosort">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($assigns as $assign) {
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $assign->device_id; ?></td>
                        <td><?php echo $assign->channel_id; ?></td>
                        <td>
                            <a href="<?php echo site_url('admin/assigns/edit') . '/' . $assign->id; ?>" class="fa fa-edit" title="Edit"></a>
                            <a href="<?php echo site_url('admin/assigns/delete') . '/' . $assign->id; ?>" onclick="return confirm('Are you sure? It will delete assign!')" class="fa fa-remove" title="Delete"></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<script>
    $(function () {
        $("#assign_table").DataTable({
            'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
        });
    });
</script>