<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Videos extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Videos_model', 'videos');
        $this->load->helper('security');
        $this->load->library('getID3/getid3/getid3');
    }

    public function index() {
        $this->mTitle = 'Videos';
        // get list of users
        $this->mViewData['videos'] = $this->videos->get_many_by(array('is_delete' => 0));
        $this->render('video/video_list');
    }

    // Create New Video
    public function create() {
        if ($this->input->post()) {
            $size_id = $this->input->post('size_id', TRUE);
            $allowed = array('mp4','MP4','3gp','3GP','flv','FLV','mkv','MKV');
            $filename = $_FILES['video_file']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if (!in_array($ext, $allowed)) {
                $errors = "Not a valid video file. Please retry.";
                $this->session->set_flashdata('error', $errors);
                redirect('admin/videos');
            } else {
                //upload configuration
                $config['upload_path'] = 'uploads/videos/' . $size_id;
                $config['allowed_types'] = 'mp4|MP4|3gp|3GP|flv|FLV|mkv|MKV';
                $this->load->library('upload', $config);
                //upload file to directory
                if ($this->upload->do_upload('video_file')) {
                    $uploadData = $this->upload->data();
                    $uploadedFile = $uploadData['file_name'];
                    $uploadedFileSize = $uploadData['file_size'];
                    $uploadedFileExt = $uploadData['file_ext'];
                    /* finding playtime (video length) starts */
                    $getID3 = new getID3;
                    $fileDetail = $getID3->analyze(VIDEO_SIZE . $size_id . '/' . $uploadedFile);
                    $vidMin = $vidSec = 0;

                    if (isset($fileDetail) && $fileDetail['playtime_string'] != '') {
                        $playtimeArr = explode(':', $fileDetail['playtime_string']);
                        if (is_array($playtimeArr) && $playtimeArr[0] > 0) {
                            $vidMin = $playtimeArr[0] * 60;
                        } elseif (is_array($playtimeArr) && $playtimeArr[1] > 0) {
                            $vidSec = $playtimeArr[1];
                        }
                        $vidPlaySeconds = $vidMin + $vidSec;
                    }
                    /* finding playtime (video length) ends */
                    $tag_ids = implode(',', $this->input->post('tag_id', TRUE));
                    // uploaded file insert in db
                    $insertVideo_Array = array(
                        'name' => $uploadedFile,
                        'size' => $uploadedFileSize,
                        'extension' => $uploadedFileExt,
                        'playtime' => ($vidPlaySeconds) ? $vidPlaySeconds : 0,
                        'tag_ids' => $tag_ids,
                        'size_id' => $this->input->post('size_id', TRUE),
                        'created_by' => $this->session->userdata('user_id'),
                        'createdtime' => date('Y-m-d H:i:s')
                    );
                    // proceed to create Video
                    $video_id = $this->videos->insert($insertVideo_Array);
                    if ($video_id) {
                        /* logging activity */
                        $activity = array(
                            'user_id' => $this->session->userdata('user_id'),
                            'activity' => $this->session->userdata('username') . " created video Name:" . $uploadedFile . " ID:" . $video_id,
                            'link' => site_url('admin/videos'),
                            'createdtime' => date('Y-m-d H:i:s')
                        );
                        $this->ion_auth->activity_log($activity);
                        /* logging activity */

                        // success
                        $messages = "Video successfully created.";
                        $this->session->set_flashdata('success', $messages);
                    } else {
                        // failed
                        $errors = "Something went wrong. Please retry.";
                        $this->session->set_flashdata('error', $errors);
                    }
                } else {
                    $errors = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $errors);
                }
                redirect('admin/videos');
            }
        }
        /* dynamic Video size (resolution) */
        $this->load->model('Sizes_model', 'sizes');
        $this->mViewData['sizes'] = $this->sizes->get_many_by(array('is_delete' => 0));
        /* dynamic tag | Video */
        $this->load->model('Tags_model', 'tags');
        $this->mViewData['tags'] = $this->tags->get_many_by(array('is_delete' => 0));
        /* dynamic tag ends */
        $this->mViewData['actionURL'] = site_url('admin/videos/create');
        $this->mTitle = 'Create Video';
        $this->render('video/create');
    }

    // Edit Video
    public function edit($video_id = 0) {
        if ($this->input->post()) {
            if ($_FILES['video_file']['size'] > 0) {
                $size_id = $this->input->post('size_id', TRUE);
                $allowed = array('mp4','MP4','3gp','3GP','flv','FLV','mkv','MKV');
                $filename = $_FILES['video_file']['name'];
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if (!in_array($ext, $allowed)) {
                    $errors = "Not a valid video file. Please retry.";
                    $this->session->set_flashdata('error', $errors);
                    redirect('admin/videos');
                } else {
                    //upload configuration
                    $config['upload_path'] = 'uploads/videos/' . $size_id;
                    $config['allowed_types'] = 'mp4|MP4|3gp|3GP|flv|FLV|mkv|MKV';
                    $this->load->library('upload', $config);
                    //upload file to directory
                    if ($this->upload->do_upload('video_file')) {
                        $uploadData = $this->upload->data();
                        $uploadedFile = $uploadData['file_name'];
                        $uploadedFileSize = $uploadData['file_size'];
                        $uploadedFileExt = $uploadData['file_ext'];
                        /* finding playtime (video length) starts */
                        $getID3 = new getID3;
                        $fileDetail = $getID3->analyze(VIDEO_SIZE . $size_id . '/' . $uploadedFile);
                        $vidMin = $vidSec = 0;

                        if (isset($fileDetail) && $fileDetail['playtime_string'] != '') {
                            $playtimeArr = explode(':', $fileDetail['playtime_string']);
                            if (is_array($playtimeArr) && $playtimeArr[0] > 0) {
                                $vidMin = $playtimeArr[0] * 60;
                            } elseif (is_array($playtimeArr) && $playtimeArr[1] > 0) {
                                $vidSec = $playtimeArr[1];
                            }
                            $vidPlaySeconds = $vidMin + $vidSec;
                        }
                        /* finding playtime (video length) ends */
                        $tag_ids = implode(',', $this->input->post('tag_id', TRUE));
                        // uploaded file update in db
                        $updateVideo_Array = array(
                            'name' => $uploadedFile,
                            'size' => $uploadedFileSize,
                            'extension' => $uploadedFileExt,
                            'playtime' => ($vidPlaySeconds) ? $vidPlaySeconds : 0,
                            'size_id' => $this->input->post('size_id', TRUE),
                            'tag_ids' => $tag_ids,
                            'updated_by' => $this->session->userdata('user_id'),
                            'updatedtime' => date('Y-m-d H:i:s')
                        );
                        // proceed to create Video
                        $updateFlag = $this->videos->update($video_id, $updateVideo_Array);
                        if ($updateFlag) {
                            //Delete old video
                            $oldFile = DELETE_VIDEO . $this->input->post('video_name');
                            unlink($oldFile);
                            /* logging activity */
                            $activity = array(
                                'user_id' => $this->session->userdata('user_id'),
                                'activity' => $this->session->userdata('username') . " edited video Name:" . $uploadedFile . " ID:" . $video_id,
                                'link' => site_url('admin/videos'),
                                'createdtime' => date('Y-m-d H:i:s')
                            );
                            $this->ion_auth->activity_log($activity);
                            /* logging activity */
                            // success
                            $messages = "Video successfully updated.";
                            $this->session->set_flashdata('success', $messages);
                        } else {
                            // failed
                            $errors = "Something went wrong. Please retry.";
                            $this->session->set_flashdata('error', $errors);
                        }
                    } else {
                        $errors = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $errors);
                    }
                    redirect('admin/videos');
                }
            }
            // success
            $messages = "Video successfully updated.";
            $this->session->set_flashdata('success', $messages);
            redirect('admin/videos');
        }
        if ($video_id != 0) {
            /* dynamic Video size (resolution) */
            $this->load->model('Sizes_model', 'sizes');
            $this->mViewData['sizes'] = $this->sizes->get_many_by(array('is_delete' => 0));
            /* dynamic tag | Video */
            $this->load->model('Tags_model', 'tags');
            $this->mViewData['tags'] = $this->tags->get_many_by(array('is_delete' => 0));
            /* dynamic tag ends */
            // get list of video
            $this->mViewData['video'] = $this->videos->get($video_id);
            $this->mTitle = 'Edit Video';
            $this->mViewData['actionURL'] = site_url('admin/videos/edit') . '/' . $video_id;
            $this->render('video/create');
        } else {
            redirect('admin/videos');
        }
    }

    // Soft Delete video
    public function delete($video_id = 0) {
        // only top-level users can reset user passwords
        $this->verify_auth(array('webmaster', 'admin'));
        // proceed to change user password
        if ($this->videos->update($video_id, array('is_delete' => 1, 'updated_by' => $this->session->userdata('user_id')))) {
            /* logging activity */
            $activity = array(
                'user_id' => $this->session->userdata('user_id'),
                'activity' => $this->session->userdata('username') . " deleted video ID:" . $video_id,
                'link' => site_url('admin/videos'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            $this->ion_auth->activity_log($activity);
            /* logging activity */
            // success
            $messages = "Video successfully deleted.";
            $this->session->set_flashdata('success', $messages);
        } else {
            // failed
            $errors = "Something went wrong. Please retry.";
            $this->session->set_flashdata('error', $errors);
        }
        redirect('admin/videos');
    }

}
