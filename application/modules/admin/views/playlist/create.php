<?php
if (!empty(validation_errors())) {
    echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
    $playlist->name = set_value('name');
}
?>
<link rel="stylesheet" href="<?php echo base_url('assets/select2/select2.min.css'); ?>">
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Playlist Info</h3>
            </div>
            <div class="box-body">
                <form name="frmPlaylist" id="frmPlaylist" method="post" action="<?php echo $actionURL; ?>">
                    <input type="hidden" name="id" value="<?php echo ($playlist->id) ? $playlist->id : ''; ?>" id="id">
                    <!-- Playlist name -->
                    <div class="form-group">
                        <label>Playlist Name*</label>
                        <input type="text" class="form-control" name="name" value="<?php echo ($playlist->name) ? $playlist->name : ''; ?>" id="value" placeholder="Enter Playlist Name.">
                    </div>
                    <!-- Select videos -->
                    <div>
                        <label for="groups">Select Videos: </label>
                        <select multiple="multiple" class="form-control js-example-basic-multiple" id="videoSelect" name="video_id[]">
                            <?php
                            $HiddenTags = explode(',', rtrim($playlist->video_ids, ','));
                            foreach ($videos as $video) {
                                $varSelect = "";
                                if (!empty(set_value('video_id'))) {
                                    if (in_array($video->id, set_value('video_id')))
                                        $varSelect = "selected";
                                } else {
                                    if (in_array($video->id, $HiddenTags))
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $video->id . "'>" . $video->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <!-- Select locations -->
                    <div>
                        <label for="groups">Select Locations: </label>
                        <select multiple="multiple" class="form-control js-example-basic-multiple" id="locationSelect" name="location_id[]">
                            <?php
                            $HiddenTags = explode(',', rtrim($playlist->location_ids, ','));
                            foreach ($locations as $location) {
                                $varSelect = "";
                                if (!empty(set_value('location_id'))) {
                                    if (in_array($location->id, set_value('location_id')))
                                        $varSelect = "selected";
                                } else {
                                    if (in_array($location->id, $HiddenTags))
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $location->id . "'>" . $location->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <!-- Playlist time -->
                    <div class="form-group timeInfo" style="<?php echo ($playlist->no_time) ? 'display:none' : '' ?>">
                        <label class="timepicker_start">Start From</label>
                        <input type="text" class="form-control" name="start_time" value="<?php echo ($playlist->start_time) ? $playlist->start_time : ''; ?>" id="value" placeholder="Playtime Allowed From (hh:ii)">
                    </div>
                    <div class="form-group timeInfo" style="<?php echo ($playlist->no_time) ? 'display:none' : '' ?>">
                        <label for="timepicker_end">End On</label>
                        <input type="text" class="form-control" name="end_time" value="<?php echo ($playlist->end_time) ? $playlist->end_time : ''; ?>" id="value" placeholder="Playtime Allowed To (hh:ii)">
                    </div>
                    <!-- Select tags -->
                    <div>
                        <label for="groups">Select Tags: </label>
                        <select multiple="multiple" class="form-control js-example-basic-multiple" id="tagSelect" name="tag_id[]">
                            <?php
                            $HiddenTags = explode(',', rtrim($playlist->tag_ids, ','));
                            foreach ($tags as $tag) {
                                $varSelect = "";
                                if (!empty(set_value('tag_id'))) {
                                    if (in_array($tag->id, set_value('tag_id')))
                                        $varSelect = "selected";
                                } else {
                                    if (in_array($tag->id, $HiddenTags))
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $tag->id . "'>" . $tag->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
            </div>
            <!-- Submit & Cancel button -->
            <div class="box-footer">
                <button type="button" name="reset" onclick="javascript:back();" class="btn btn-default">Back</button>
                <button type="submit" name="submit" onclick="javascript:submitForm();" class="btn btn-info pull-right">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<script src="<?php echo base_url('assets/select2/select2.min.js'); ?>"></script>
<script>
//select2 for user selection
    $(".js-example-basic-multiple").select2();
//submitting form on button click.
    function back() {
        window.playlist.href = '<?php echo site_url('admin/playlists'); ?>';
    }
//submitting form on button click.
    function submitForm() {
        $("#frmPlaylist").submit();
    }
</script>