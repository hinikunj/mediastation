<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | CI Bootstrap 3 Configuration
  | -------------------------------------------------------------------------
  | This file lets you define default values to be passed into views
  | when calling MY_Controller's render() function.
  |
  | See example and detailed explanation from:
  | 	/application/config/ci_bootstrap_example.php
 */

$config['ci_bootstrap'] = array(
    // Site name
    'site_name' => 'Admin Panel',
    // Default page title prefix
    'page_title_prefix' => '',
    // Default page title
    'page_title' => '',
    // Default meta data
    'meta_data' => array(
        'author' => '',
        'description' => '',
        'keywords' => ''
    ),
    // Default scripts to embed at page head or end
    'scripts' => array(
        'head' => array(
            'assets/dist/admin/adminlte.min.js',
            'assets/dist/admin/lib.min.js',
            'assets/dist/admin/app.min.js',
            'assets/datatables/jquery.dataTables.min.js',
            'assets/datatables/dataTables.bootstrap.min.js'
        ),
        'foot' => array(
        ),
    ),
    // Default stylesheets to embed at page head
    'stylesheets' => array(
        'screen' => array(
            'assets/dist/admin/adminlte.min.css',
            'assets/dist/admin/lib.min.css',
            'assets/dist/admin/app.min.css',
            'assets/datatables/dataTables.bootstrap.css'
        )
    ),
    // Default CSS class for <body> tag
    'body_class' => '',
    // Multilingual settings
    'languages' => array(
    ),
    // Menu items
    'menu' => array(
        'home' => array(
            'name' => 'Home',
            'url' => '',
            'icon' => 'fa fa-home',
        ),
        /* 'user' => array(
          'name' => 'Users',
          'url' => 'user',
          'icon' => 'fa fa-users',
          'children' => array(
          'List' => 'user',
          'Create' => 'user/create',
          'User Groups' => 'user/group',
          )
          ), */
        'panel' => array(
            'name' => 'Admin Panel',
            'url' => 'panel',
            'icon' => 'fa fa-cog',
            'children' => array(
                'Admin Users' => 'panel/admin_user',
                'Create Admin User' => 'panel/admin_user_create',
            #'Admin User Groups' => 'panel/admin_user_group',
            )
        ),
        'tags' => array(
            'name' => 'Tags',
            'url' => 'tags',
            'icon' => 'fa fa-tag',
            'children' => array(
                'Tag List' => 'tags/',
                'Create Tag' => 'tags/create',
            )
        ),
        'videos' => array(
            'name' => 'Videos',
            'url' => 'videos',
            'icon' => 'fa fa-video-camera',
            'children' => array(
                'Video List' => 'videos/',
                'Create Video' => 'videos/create',
                'Size List' => 'sizes/',
                'Create Size' => 'sizes/create',
            )
        ),
        'locations' => array(
            'name' => 'Locations',
            'url' => 'locations',
            'icon' => 'fa fa-location-arrow',
            'children' => array(
                'Location List' => 'locations/index',
                'Create Location' => 'locations/create',
            )
        ),
        'devices' => array(
            'name' => 'Devices',
            'url' => 'devices',
            'icon' => 'fa fa-mobile',
            'children' => array(
                'Device List' => 'devices/',
                'Create Device' => 'devices/create',
            )
        ),
        'channels' => array(
            'name' => 'Channel',
            'url' => 'channels',
            'icon' => 'fa fa-tv',
            'children' => array(
                'Channel List' => 'channels/',
                'Create Channel' => 'channels/create',
                'Channel Video List' => 'channelsvideo/',
                'Create Channel Video' => 'channelsvideo/create',
            )
        ),
        'assigns' => array(
            'name' => 'Assign Devices',
            'url' => 'assigns',
            'icon' => 'fa fa-exchange',
            'children' => array(
                'Assigned List' => 'assigns/index',
                'Create' => 'assigns/create',
            )
        ),
        'prices' => array(
            'name' => 'Prices',
            'url' => 'prices',
            'icon' => 'fa fa-rupee',
            'children' => array(
                'Price List' => 'prices/index',
                'Create' => 'prices/create',
            )
        ),
        'masters' => array(
            'name' => 'Masters',
            'url' => 'masters',
            'icon' => 'fa fa-table',
            'children' => array(
                'Area List' => 'areas/',
                'Create Area' => 'areas/create',
                'City List' => 'cities/',
                'Create City' => 'cities/create',
                'State List' => 'states/',
                'Create State' => 'states/create',
            )
        ),
        /* 'util' => array(
          'name' => 'Utilities',
          'url' => 'util',
          'icon' => 'fa fa-cogs',
          'children' => array(
          'Database Versions' => 'util/list_db',
          )
          ), */
        'logout' => array(
            'name' => 'Sign Out',
            'url' => 'panel/logout',
            'icon' => 'fa fa-sign-out',
        )
    ),
    // Login page
    'login_url' => 'admin/login',
    // Restricted pages
    'page_auth' => array(
        'user/create' => array('webmaster', 'admin', 'manager'),
        'user/group' => array('webmaster', 'admin', 'manager'),
        'panel' => array('webmaster'),
        'panel/admin_user' => array('webmaster'),
        'panel/admin_user_create' => array('webmaster'),
        'panel/admin_user_group' => array('webmaster'),
        'util' => array('webmaster'),
        'util/list_db' => array('webmaster'),
        'util/backup_db' => array('webmaster'),
        'util/restore_db' => array('webmaster'),
        'util/remove_db' => array('webmaster'),
    ),
    // AdminLTE settings
    'adminlte' => array(
        'body_class' => array(
            'webmaster' => 'skin-red',
            'admin' => 'skin-purple',
            'manager' => 'skin-black',
            'staff' => 'skin-blue',
        )
    ),
    // Useful links to display at bottom of sidemenu
    'useful_links' => array(
        /* array(
          'auth' => array('webmaster', 'admin', 'manager', 'staff'),
          'name' => 'Frontend Website',
          'url' => '',
          'target' => '_blank',
          'color' => 'text-aqua'
          ),
          array(
          'auth' => array('webmaster', 'admin'),
          'name' => 'API Site',
          'url' => 'api',
          'target' => '_blank',
          'color' => 'text-orange'
          ), */
        array(
            'auth' => array('webmaster', 'admin', 'manager', 'staff'),
            'name' => 'Github Repo',
            'url' => CI_BOOTSTRAP_REPO,
            'target' => '_blank',
            'color' => 'text-green'
        ),
    ),
    // Debug tools
    'debug' => array(
        'view_data' => FALSE,
        'profiler' => FALSE
    ),
);

/*
  | -------------------------------------------------------------------------
  | Override values from /application/config/config.php
  | -------------------------------------------------------------------------
 */
$config['sess_cookie_name'] = 'ci_session_admin';
