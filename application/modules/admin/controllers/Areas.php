<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Areas extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Areas_model', 'areas');
        $this->load->helper('security');
    }

    public function index() {
        $this->mTitle = 'Areas';
        // get list of users
        $this->mViewData['areas'] = $this->areas->get_many_by(array('is_delete' => 0));
        $this->render('area/area_list');
    }

    // Create New Area
    public function create() {
        /* if($_POST){
          echo "<pre>";
          print_r($this->input->post());
          echo "</pre>";
          exit();
          } */
        $this->form_validation->set_rules('name', 'Area name', 'required|callback_unique_area');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $insertArea_Array = array(
                'name' => $this->input->post('name', TRUE),
                'created_by' => $this->session->userdata('user_id'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            // proceed to create Area
            $area_id = $this->areas->insert($insertArea_Array);
            if ($area_id) {
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " created area Name:" . $this->input->post('name', TRUE) . " ID:" . $area_id,
                    'link' => site_url('admin/areas'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */

                // success
                $messages = "Area successfully created.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/areas');
        }
        $this->mViewData['actionURL'] = site_url('admin/areas/create');
        $this->mTitle = 'Create Area';
        $this->render('area/create');
    }

    // Edit Area
    public function edit($area_id = 0) {
        $this->form_validation->set_rules('name', 'Area name', 'required|callback_unique_area');
        if ($this->form_validation->run($this) == TRUE) {
            // passed validation
            $updateAreaArray = array(
                'name' => $this->input->post('name', TRUE),
                'updated_by' => $this->session->userdata('user_id'),
                'updatedtime' => date('Y-m-d H:i:s')
            );
            // proceed to update area
            $updateFlag = $this->areas->update($area_id, $updateAreaArray);
            if ($updateFlag) {
                // success
                /* logging activity */
                $activity = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'activity' => $this->session->userdata('username') . " edited area Name:" . $this->input->post('name', TRUE) . " ID:" . $area_id,
                    'link' => site_url('admin/areas'),
                    'createdtime' => date('Y-m-d H:i:s')
                );
                $this->ion_auth->activity_log($activity);
                /* logging activity */
                $messages = "Area successfully updated.";
                $this->session->set_flashdata('success', $messages);
            } else {
                // failed
                $errors = "Something went wrong. Please retry.";
                $this->session->set_flashdata('error', $errors);
            }
            redirect('admin/areas');
        }

        if ($area_id != 0) {
            // get list of area
            $this->mViewData['area'] = $this->areas->get($area_id);
            $this->mTitle = 'Edit Area';
            $this->mViewData['actionURL'] = site_url('admin/areas/edit') . '/' . $area_id;
            $this->render('area/create');
        } else {
            redirect('admin/areas');
        }
    }

    // Soft Delete area
    public function delete($area_id = 0) {
        // only top-level users can reset user passwords
        $this->verify_auth(array('webmaster', 'admin'));
        // proceed to change user password
        if ($this->areas->update($area_id, array('is_delete' => 1, 'updated_by' => $this->session->userdata('user_id')))) {
            /* logging activity */
            $activity = array(
                'user_id' => $this->session->userdata('user_id'),
                'activity' => $this->session->userdata('username') . " deleted area ID:" . $area_id,
                'link' => site_url('admin/areas'),
                'createdtime' => date('Y-m-d H:i:s')
            );
            $this->ion_auth->activity_log($activity);
            /* logging activity */
            // success
            $messages = "Area successfully deleted.";
            $this->session->set_flashdata('success', $messages);
        } else {
            // failed
            $errors = "Something went wrong. Please retry.";
            $this->session->set_flashdata('error', $errors);
        }
        redirect('admin/areas');
    }

    //callback for unique area name
    public function unique_area($str) {
        if ($this->input->post('id', TRUE))
            $this->db->where_not_in('id', $this->input->post('id', TRUE));
        $this->db->where('name', $str);
        if ($this->db->count_all_results('areas') > 0) {
            $this->form_validation->set_message('unique_area', 'The {field} field must contain a unique value.');
            return FALSE;
        } else {
            return true;
        }
    }

}
