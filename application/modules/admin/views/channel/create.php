<?php
if (!empty(validation_errors())) {
    echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
    $channel->name = set_value('name');
    $channel->time = set_value('time');
}
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Channel Info</h3>
            </div>
            <div class="box-body">
                <form name="frmChannel" id="frmChannel" method="post" action="<?php echo $actionURL; ?>">
                    <input type="hidden" name="id" value="<?php echo ($channel->id) ? $channel->id : ''; ?>" id="id">
                    <!-- Channel name -->
                    <div class="form-group">
                        <label>Channel Name*</label>
                        <input type="text" class="form-control" name="name" value="<?php echo ($channel->name) ? $channel->name : ''; ?>" id="value" placeholder="Enter Channel Name.">
                    </div>
                    <!-- Select size -->
                    <div>
                        <label for="groups">Select Size: </label>
                        <select class="form-control js-example-basic" id="sizeSelect" name="size_id">
                            <option value=''>--Select size--</option>
                            <?php
                            foreach ($sizes as $size) {
                                $varSelect = "";
                                if (!empty(set_value('size_id'))) {
                                    if (in_array($size->id, set_value('size_id')))
                                        $varSelect = "selected";
                                } else {
                                    if ($size->id == $channel->size_id)
                                        $varSelect = "selected";
                                }
                                echo "<option $varSelect value='" . $size->id . "'>" . $size->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <!-- Channel time -->
                    <div class="form-group">
                        <label>Channel Time*</label>
                        <input type="text" class="form-control" name="time" value="<?php echo ($channel->time) ? $channel->time : ''; ?>" id="value" placeholder="Enter Channel Time (secods).">
                    </div>
                    <!-- Submit & Cancel button -->
                    <div class="box-footer">
                        <button type="button" name="reset" onclick="javascript:back();" class="btn btn-default">Back</button>
                        <button type="submit" name="submit" onclick="javascript:submitForm();" class="btn btn-info pull-right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
//submitting form on button click.
    function back() {
        window.location.href = '<?php echo site_url('admin/channels'); ?>';
    }
//submitting form on button click.
    function submitForm() {
        $("#frmChannel").submit();
    }
</script>